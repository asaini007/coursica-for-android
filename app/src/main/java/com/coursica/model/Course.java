package com.coursica.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Course extends RealmObject {
    @Required
    private String level;
    @Required
    private String catalogNumber;
    @Required
    private String courseDescription;
    @Required
    private String examGroup;
    private boolean spring;
    private boolean fall;
    private int enrollment;
    @Required
    private String enrollmentSource;
    @Required
    private String school;
    @Required
    private String department;
    @Required
    private String credits;
    @Required
    private String longField;
    @Required
    private String shortField;
    @Required
    private String notes;
    @Required
    private String number;
    @Required
    private String title;
    @Required
    private String prerequisitesString;
    private int integerNumber;
    private double overall;
    private double workload;
    private double difficulty;
    private double searchScore;
    private RealmList<GenEd> genEds;
    private RealmList<Faculty> faculty;
    private RealmList<Meeting> meetings;
    private RealmList<Course> prerequisites;
    private RealmList<Offering> offerings;

    public String getLevel() { return level; }

    public void setLevel(String level) { this.level = level; } 

    public String getCatalogNumber() { return catalogNumber; }

    public void setCatalogNumber(String catalogNumber) { this.catalogNumber = catalogNumber; }

    public String getCourseDescription() { return courseDescription; }

    public void setCourseDescription(String courseDescription) { this.courseDescription = courseDescription; }

    public String getExamGroup() { return examGroup; }

    public void setExamGroup(String examGroup) { this.examGroup = examGroup; }

    public boolean isSpring() { return spring; }

    public void setSpring(boolean spring) { this.spring = spring; } 

    public boolean isFall() { return fall; }

    public void setFall(boolean fall) { this.fall = fall; } 

    public int getEnrollment() { return enrollment; }

    public void setEnrollment(int enrollment) { this.enrollment = enrollment; } 

    public String getEnrollmentSource() { return enrollmentSource; }

    public void setEnrollmentSource(String enrollmentSource) { this.enrollmentSource = enrollmentSource; }

    public String getSchool() { return school; }

    public void setSchool(String school) { this.school = school; } 

    public String getDepartment() { return department; }

    public void setDepartment(String department) { this.department = department; } 

    public String getCredits() { return credits; }

    public void setCredits(String credits) { this.credits = credits; } 

    public String getLongField() { return longField; }

    public void setLongField(String longField) { this.longField = longField; }

    public String getShortField() { return shortField; }

    public void setShortField(String shortField) { this.shortField = shortField; }

    public String getNotes() { return notes; }

    public void setNotes(String notes) { this.notes = notes; } 

    public String getNumber() { return number; }

    public void setNumber(String number) { this.number = number; } 

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; } 

    public String getPrerequisitesString() { return prerequisitesString; }

    public void setPrerequisitesString(String prerequisitesString) { this.prerequisitesString = prerequisitesString; }

    public int getIntegerNumber() { return integerNumber; }

    public void setIntegerNumber(int integerNumber) { this.integerNumber = integerNumber; }

    public double getOverall() { return overall; }

    public void setOverall(double overall) { this.overall = overall; } 

    public double getWorkload() { return workload; }

    public void setWorkload(double workload) { this.workload = workload; } 

    public double getDifficulty() { return difficulty; }

    public void setDifficulty(double difficulty) { this.difficulty = difficulty; } 

    public double getSearchScore() { return searchScore; }

    public void setSearchScore(double searchScore) { this.searchScore = searchScore; }

    public RealmList<GenEd> getGenEds() { return genEds; }

    public void setGenEds(RealmList<GenEd> genEds) { this.genEds = genEds; }

    public RealmList<Faculty> getFaculty() { return faculty; }

    public void setFaculty(RealmList<Faculty> faculty) { this.faculty = faculty; } 

    public RealmList<Meeting> getMeetings() { return meetings; }

    public void setMeetings(RealmList<Meeting> meetings) { this.meetings = meetings; } 

    public RealmList<Course> getPrerequisites() { return prerequisites; }

    public void setPrerequisites(RealmList<Course> prerequisites) { this.prerequisites = prerequisites; } 

    public RealmList<Offering> getOfferings() { return offerings; }

    public void setOfferings(RealmList<Offering> offerings) { this.offerings = offerings; } 

}
