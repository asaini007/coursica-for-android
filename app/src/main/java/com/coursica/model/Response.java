package com.coursica.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Response extends RealmObject {
    @Required
    private String question;
    private Breakdown breakdown;
    private Baseline baselineSingleTerm;
    private Baseline baselineThreeYears;
    private double mean;
    private int median;

    public String getQuestion() { return question; }

    public void setQuestion(String question) { this.question = question; } 

    public Breakdown getBreakdown() { return breakdown; }

    public void setBreakdown(Breakdown breakdown) { this.breakdown = breakdown; } 

    public Baseline getBaselineSingleTerm() { return baselineSingleTerm; }

    public void setBaselineSingleTerm(Baseline baselineSingleTerm) { this.baselineSingleTerm = baselineSingleTerm; }

    public Baseline getBaselineThreeYears() { return baselineThreeYears; }

    public void setBaselineThreeYears(Baseline baselineThreeYears) { this.baselineThreeYears = baselineThreeYears; }

    public double getMean() { return mean; }

    public void setMean(double mean) { this.mean = mean; } 

    public int getMedian() { return median; }

    public void setMedian(int median) { this.median = median; } 

}
