package com.coursica.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

public class FacultyReport extends RealmObject {
    @Required
    private String name;
    private RealmList<Response> responses;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; } 

    public RealmList<Response> getResponses() { return responses; }

    public void setResponses(RealmList<Response> responses) { this.responses = responses; } 

}
