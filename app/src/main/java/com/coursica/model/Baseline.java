package com.coursica.model;

import io.realm.RealmObject;

public class Baseline extends RealmObject {
    private double department;
    private double division;
    private double group;
    private double size;

    public double getDepartment() { return department; }

    public void setDepartment(double department) { this.department = department; } 

    public double getDivision() { return division; }

    public void setDivision(double division) { this.division = division; } 

    public double getGroup() { return group; }

    public void setGroup(double group) { this.group = group; } 

    public double getSize() { return size; }

    public void setSize(double size) { this.size = size; } 

}
