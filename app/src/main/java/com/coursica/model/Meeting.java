package com.coursica.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Meeting extends RealmObject {
    @Required
    private String day;
    @Required
    private String beginTime;
    @Required
    private String endTime;
    private Offering offering;

    public String getDay() { return day; }

    public void setDay(String day) { this.day = day; } 

    public String getBeginTime() { return beginTime; }

    public void setBeginTime(String beginTime) { this.beginTime = beginTime; }

    public String getEndTime() { return endTime; }

    public void setEndTime(String endTime) { this.endTime = endTime; }

    public Offering getOffering() { return offering; }

    public void setOffering(Offering offering) { this.offering = offering; } 

}
