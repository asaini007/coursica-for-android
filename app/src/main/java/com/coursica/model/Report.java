package com.coursica.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Report extends RealmObject {
    @Required
    private String term;
    @Required
    private String year;
    private int enrollment;
    private RealmList<Response> responses;
    private RealmList<FacultyReport> facultyReports;

    public String getTerm() { return term; }

    public void setTerm(String term) { this.term = term; } 

    public String getYear() { return year; }

    public void setYear(String year) { this.year = year; } 

    public int getEnrollment() { return enrollment; }

    public void setEnrollment(int enrollment) { this.enrollment = enrollment; } 

    public RealmList<Response> getResponses() { return responses; }

    public void setResponses(RealmList<Response> responses) { this.responses = responses; } 

    public RealmList<FacultyReport> getFacultyReports() { return facultyReports; }

    public void setFacultyReports(RealmList<FacultyReport> facultyReports) { this.facultyReports = facultyReports; }

}
