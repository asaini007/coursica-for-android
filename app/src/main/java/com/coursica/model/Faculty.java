package com.coursica.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Faculty extends RealmObject {
    @Required
    private String first;
    @Required
    private String middle;
    @Required
    private String last;
    @Required
    private String suffix;
    private RealmList<Course> courses;

    public String getFirst() { return first; }

    public void setFirst(String first) { this.first = first; } 

    public String getMiddle() { return middle; }

    public void setMiddle(String middle) { this.middle = middle; } 

    public String getLast() { return last; }

    public void setLast(String last) { this.last = last; } 

    public String getSuffix() { return suffix; }

    public void setSuffix(String suffix) { this.suffix = suffix; } 

    public RealmList<Course> getCourses() { return courses; }

    public void setCourses(RealmList<Course> courses) { this.courses = courses; } 

}
