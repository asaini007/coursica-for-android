package com.coursica.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class FacultyAverage extends RealmObject {
    @Required
    private String question;
    private double score;

    public String getQuestion() { return question; }

    public void setQuestion(String question) { this.question = question; } 

    public double getScore() { return score; }

    public void setScore(double score) { this.score = score; } 

}
