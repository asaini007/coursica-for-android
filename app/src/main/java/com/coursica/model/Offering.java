package com.coursica.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Offering extends RealmObject {
    @Required
    private String term;
    @Required
    private String year;
    @Required
    private String latitude;
    @Required
    private String longitude;
    @Required
    private String location;
    private RealmList<Faculty> faculty;
    private RealmList<Meeting> meetings;

    public String getTerm() { return term; }

    public void setTerm(String term) { this.term = term; } 

    public String getYear() { return year; }

    public void setYear(String year) { this.year = year; } 

    public String getLatitude() { return latitude; }

    public void setLatitude(String latitude) { this.latitude = latitude; } 

    public String getLongitude() { return longitude; }

    public void setLongitude(String longitude) { this.longitude = longitude; } 

    public String getLocation() { return location; }

    public void setLocation(String location) { this.location = location; } 

    public RealmList<Faculty> getFaculty() { return faculty; }

    public void setFaculty(RealmList<Faculty> faculty) { this.faculty = faculty; } 

    public RealmList<Meeting> getMeetings() { return meetings; }

    public void setMeetings(RealmList<Meeting> meetings) { this.meetings = meetings; } 

}
