package com.coursica.fragments;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.coursica.activities.DetailedBreakdownActivity;
import com.coursica.coursica.R;
import com.coursica.helpers.Animatable;
import com.coursica.helpers.Color;
import com.coursica.helpers.FirebaseHelper;
import com.coursica.helpers.FirebaseHelper.CategoryData;
import com.coursica.helpers.FirebaseHelper.InstructorData;
import com.coursica.helpers.FirebaseHelper.TermData;
import com.coursica.model.Course;
import com.coursica.views.BarView;
import com.coursica.views.HistogramView;
import com.coursica.views.ScoreView;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ScoresFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private static final String TAG = "ScoresFragment";
    public static final String BREAKDOWN_TYPE = "com.coursica.coursica.BREAKDOWN_TYPE";
    public static final String BREAKDOWN_DATA = "com.coursica.coursica.BREAKDOWN_DATA";
    public static final String BREAKDOWN_LABELS = "com.coursica.coursica.BREAKDOWN_LABELS";
    public static final String BREAKDOWN_QUESTION = "com.coursica.coursica.BREAKDOWN_QUESTION";

    Course course;
    List<TermData> courseData = null;
    List<Float> histData = null;
    ArrayList<String> terms;
    List<Animatable> animatables;
    int selectedTerm, selectedScoreType, selectedCourseType;

    Map<String, Integer> categoriesToBarIds = new HashMap<>();
    Map<String, Integer> categoriesToScoreTextIds = new HashMap<>();

    public static ScoresFragment newInstance(String number, String shortField, String title) {
        Bundle args = new Bundle();
        args.putString("number", number);
        args.putString("shortField", shortField);
        args.putString("title", title);
        ScoresFragment fragment = new ScoresFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String number = getArguments().getString("number");
        String shortField = getArguments().getString("shortField");
        String title = getArguments().getString("title");

        Realm realm;
        try {
            realm = Realm.getDefaultInstance();
        } catch(NullPointerException e) {
            Firebase.setAndroidContext(getContext());
            RealmConfiguration realmConfig = new RealmConfiguration.Builder(getContext())
                    .name("courses.realm")
                    .build();
            Realm.setDefaultConfiguration(realmConfig);
            realm = Realm.getDefaultInstance();
        }
        this.course = realm.where(Course.class)
                .equalTo("number", number)
                .equalTo("shortField", shortField)
                .equalTo("title", title)
                .findFirst();

        final String childName = FirebaseHelper.courseChildName(shortField, number);
        Firebase fb = FirebaseHelper.getCourseRef(childName);
        fb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                initialize(dataSnapshot);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void initialize(DataSnapshot dataSnapshot) {
        courseData = FirebaseHelper.getTermData(dataSnapshot);
        terms = new ArrayList<>();
        for (TermData termData : courseData) {
            String term = termData.getTerm().substring(0, 1).toUpperCase() + termData.getTerm().substring(1);
            String year = termData.getYear();
            terms.add(term + " " + year);
        }
        selectedTerm = selectedCourseType = selectedScoreType = 0;
        refresh();
    }

    private void refresh() {
        android.support.v4.app.FragmentManager fm = getFragmentManager();
        if(fm != null) {
            Fragment currentFragment = getFragmentManager().findFragmentByTag(getTag());
            FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
            fragTransaction.detach(currentFragment);
            fragTransaction.attach(currentFragment);
            fragTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(courseData == null) {
            return inflater.inflate(R.layout.loading, container, false);
        } else if(courseData.isEmpty()) {
            View mainView = inflater.inflate(R.layout.text_view_course, container, false);
            ((TextView) mainView.findViewById(R.id.centered_text_view)).setText(R.string.data_not_in_firebase_message);
            return mainView;
        } else {
            View mainView = inflater.inflate(R.layout.fragment_scores, container, false);
            final ScrollView scrollView = (ScrollView) mainView.findViewById(R.id.scores_fragment_scroll_view);
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    Log.d(TAG, "Scroll Changed");
                    animateVisibleAnimatibles();
                }
            });
            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Log.d(TAG, "Layout changed");
                    animateVisibleAnimatibles();
                }
            });

            animatables = new ArrayList<>();
            setUpSpinners(mainView);
            setUpScoreViews(mainView);
            setHistogramData(mainView);
            setUpHistogramView(mainView);
            setUpDetailedBreakdowns(mainView);

            return mainView;
        }
    }

    // returns whether given animatable is visible in given scrollView; if so, animates it
    private boolean animateIfVisible(View scrollView, Animatable animatable) {
        Rect scrollBounds = new Rect();
        scrollView.getHitRect(scrollBounds);

        boolean visible = animatable.getGlobalVisibleRect(scrollBounds);
        Log.d(TAG, animatable.toString() + " visible? " + visible);
        if(visible) {
            animatable.runAnimation();
        }
        return visible;
    }

    // animates given animatable immediately if visible; otherwise, adds it to animatables
    private void animateWhenVisible(Animatable animatable) {
        View mainView = getView();
        if(mainView == null) {
            animatables.add(animatable);
            return;
        }

        ScrollView scrollView = (ScrollView) mainView.findViewById(R.id.scores_fragment_scroll_view);
        boolean visible = animateIfVisible(scrollView, animatable);
        if(!visible) {
            animatables.add(animatable);
        }
    }

    // animates all visible animatables
    private void animateVisibleAnimatibles() {
        View mainView = getView();
        if(mainView == null) {
            return;
        }

        ScrollView scrollView = (ScrollView) mainView.findViewById(R.id.scores_fragment_scroll_view);
        Rect scrollBounds = new Rect();
        scrollView.getHitRect(scrollBounds);

        Iterator<Animatable> iterator = animatables.iterator();
        while(iterator.hasNext()) {
            Animatable animatable = iterator.next();
            boolean visible = animateIfVisible(scrollView, animatable);
            if(visible) {
                iterator.remove();
            }
        }
    }

    private void setUpSpinners(View mainView) {
        Spinner termsSpinner = (Spinner) mainView.findViewById(R.id.scores_fragment_term_spinner);
        ArrayAdapter<String> termsAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item_term, terms);
        termsSpinner.setAdapter(termsAdapter);
        termsSpinner.setOnItemSelectedListener(this);

        Spinner histScoreTypeSpinner = (Spinner) mainView.findViewById(R.id.hist_score_type);
        ArrayList<String> histScoreTypes =  new ArrayList<>(Arrays.asList("Overall", "Workload"));
        ArrayAdapter<String> histScoreTypeAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item_histogram, histScoreTypes);
        histScoreTypeSpinner.setAdapter(histScoreTypeAdapter);
        histScoreTypeSpinner.setOnItemSelectedListener(this);

        Spinner histCourseTypeSpinner = (Spinner) mainView.findViewById(R.id.hist_course_type);
        ArrayList<String> histCourseTypes = new ArrayList<>(Arrays.asList("Group", "Size", "All"));
        ArrayAdapter<String> histCourseTypeAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item_histogram, histCourseTypes);
        histCourseTypeSpinner.setAdapter(histCourseTypeAdapter);
        histCourseTypeSpinner.setOnItemSelectedListener(this);
    }

    private void setUpScoreViews(View mainView) {
        if(mainView == null) {
            mainView = getView();
        }

        TermData term = courseData.get(selectedTerm);

        float overall = term.getOverallScore();
        ScoreView overallView = (ScoreView) mainView.findViewById(R.id.overall_scoreview);
        if(overall >= 0) {
            overallView.setVisibility(View.VISIBLE);
            float overallBaseline = term.getOverallBaseline();
            overallView.set(overall, "Overall", Color.getColorBaseline(getContext(), overall, overallBaseline));
            overallView.setOnClickListener(this);
            animateWhenVisible(overallView);
        } else {
            overallView.setVisibility(View.GONE);
        }

        float workload = term.getWorkloadScore();
        ScoreView workloadView = (ScoreView) mainView.findViewById(R.id.workload_scoreview);
        if(workload >= 0) {
            workloadView.setVisibility(View.VISIBLE);
            float workloadBaseline = term.getWorkloadBaseline();
            workloadView.set(workload, "Workload", Color.getInverseColorBaseline(getContext(), workload, workloadBaseline));
            workloadView.setOnClickListener(this);
            animateWhenVisible(workloadView);
        } else {
            workloadView.setVisibility(View.GONE);
        }

        TextView instructorTextView = (TextView) mainView.findViewById(R.id.instructor_name);
        ScoreView instructorScoreView = (ScoreView) mainView.findViewById(R.id.instructor_scoreview);
        List<InstructorData> instructors = term.getInstructors();
        if (instructors.size() == 1) {
            InstructorData instructorData = instructors.get(0);
            instructorTextView.setText(instructorData.getName());
            float instructor = instructorData.getMean();
            if(instructor >= 0) {
                float instructorBaseline = instructorData.getBaseline();
                instructorScoreView.setVisibility(View.VISIBLE);
                instructorScoreView.set(instructor, "Instructor", Color.getColorBaseline(getContext(), instructor, instructorBaseline));
                animateWhenVisible(instructorScoreView);
            }
        } else {
            instructorTextView.setText(instructors.size() + getResources().getString(R.string.faculty_plural));
            instructorScoreView.setVisibility(View.GONE);
        }

        mainView.requestLayout();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.scores_fragment_term_spinner:
                if(selectedTerm != position) {
                    selectedTerm = position;
                    setUpScoreViews(null);
                    setHistogramData(null);
                    setUpHistogramView(null);
                    setUpDetailedBreakdowns(null);
                }
                break;
            case R.id.hist_score_type:
                if(selectedScoreType != position) {
                    selectedScoreType = position;
                    setHistogramData(null);
                    setUpHistogramView(null);
                }
                break;
            case R.id.hist_course_type:
                if(selectedCourseType != position) {
                    selectedCourseType = position;
                    setHistogramData(null);
                    setUpHistogramView(null);
                }
                break;
        }
    }

    private void setHistogramData(View mainView) {
        if(mainView == null) {
            mainView = getView();
        }

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Course> histCourses;

        Spinner histScoreTypeSpinner = (Spinner) mainView.findViewById(R.id.hist_score_type);
        Spinner histCourseTypeSpinner = (Spinner) mainView.findViewById(R.id.hist_course_type);

        if(histCourseTypeSpinner.getSelectedItem().toString().equals("Group")) {
            histCourses = realm.where(Course.class)
                    .equalTo("shortField", course.getShortField())
                    .findAll();
        } else if(histCourseTypeSpinner.getSelectedItem().toString().equals("Size")) {
            int size = course.getEnrollment();
            if(size <= 5) {
                histCourses = realm.where(Course.class).between("enrollment", 1, 5).findAll();
            } else if (size <= 10) {
                histCourses = realm.where(Course.class).between("enrollment", 6, 10).findAll();
            } else if (size <= 20) {
                histCourses = realm.where(Course.class).between("enrollment", 11, 20).findAll();
            } else if (size <= 40) {
                histCourses = realm.where(Course.class).between("enrollment", 21, 40).findAll();
            } else if (size <= 80) {
                histCourses = realm.where(Course.class).between("enrollment", 41, 80).findAll();
            } else if (size <= 160) {
                histCourses = realm.where(Course.class).between("enrollment", 81, 160).findAll();
            } else {
                histCourses = realm.where(Course.class).greaterThanOrEqualTo("enrollment", 161).findAll();
            }
        } else {
            histCourses = realm.where(Course.class).findAll();
        }

        histData = new ArrayList<>();
        if(histScoreTypeSpinner.getSelectedItem().toString().equals("Overall")) {
            for(Course c : histCourses) {
                if(c.getOverall() > 0) {
                    histData.add((float) c.getOverall());
                }
            }
        } else if(histScoreTypeSpinner.getSelectedItem().toString().equals("Workload")) {
            for(Course c : histCourses) {
                if(c.getWorkload() > 0) {
                    histData.add((float) c.getWorkload());
                }
            }
        }
    }

    private void setUpHistogramView(View mainView) {
        if(histData != null) {
            if(mainView == null) {
                mainView = getView();
            }

            HistogramView histogramView = (HistogramView) mainView.findViewById(R.id.histogram);
            Spinner histScoreTypeSpinner = (Spinner) mainView.findViewById(R.id.hist_score_type);

            TermData term = courseData.get(selectedTerm);
            float selectedValue, selectedBaseline;
            int selectedColor;
            if (histScoreTypeSpinner.getSelectedItem().toString().equals("Overall")) {
                selectedValue = term.getOverallScore();
                selectedBaseline = term.getOverallBaseline();
                selectedColor = Color.getColorBaseline(getContext(), selectedValue, selectedBaseline);
            } else {
                selectedValue = term.getWorkloadScore();
                selectedBaseline = term.getWorkloadBaseline();
                selectedColor = Color.getInverseColorBaseline(getContext(), selectedValue, selectedBaseline);
            }
            histogramView.set(histData, selectedValue, selectedColor);
            animateWhenVisible(histogramView);
        }
    }

    private void setUpDetailedBreakdowns(View mainView) {
        if(mainView == null) {
            mainView = getView();
        }

        categoriesToBarIds.put("Instructor Overall", R.id.overall_bar);
        categoriesToBarIds.put("Effective Lectures or Presentations", R.id.lectures_bar);
        categoriesToBarIds.put("Accessible Outside Class", R.id.accessible_bar);
        categoriesToBarIds.put("Generates Enthusiasm", R.id.enthusiasm_bar);
        categoriesToBarIds.put("Facilitates Discussion & Encourages Participation", R.id.discussion_bar);
        categoriesToBarIds.put("Gives Useful Feedback", R.id.feedback_bar);
        categoriesToBarIds.put("Returns Assignments in Timely Fashion", R.id.turnaround_bar);

        categoriesToScoreTextIds.put("Instructor Overall", R.id.overall_score);
        categoriesToScoreTextIds.put("Effective Lectures or Presentations", R.id.lectures_score);
        categoriesToScoreTextIds.put("Accessible Outside Class", R.id.accessible_score);
        categoriesToScoreTextIds.put("Generates Enthusiasm", R.id.enthusiasm_score);
        categoriesToScoreTextIds.put("Facilitates Discussion & Encourages Participation", R.id.discussion_score);
        categoriesToScoreTextIds.put("Gives Useful Feedback", R.id.feedback_score);
        categoriesToScoreTextIds.put("Returns Assignments in Timely Fashion", R.id.turnaround_score);

        LinearLayout linearLayout = (LinearLayout) mainView.findViewById(R.id.scores_linear_layout);

        while(linearLayout.findViewById(R.id.instructor_breakdown) != null) {
            linearLayout.removeView(linearLayout.findViewById(R.id.instructor_breakdown));
        }

        TermData term = courseData.get(selectedTerm);
        List<InstructorData> instructors = term.getInstructors();
        for(InstructorData instructorData : instructors) {
            View child = getActivity().getLayoutInflater().inflate(R.layout.instructor_cardview, linearLayout, false);
            TextView name = (TextView) child.findViewById(R.id.name);
            name.setText(instructorData.getName());
            for(CategoryData categoryData : instructorData.getCategories()) {
                if(categoriesToScoreTextIds.containsKey(categoryData.categoryName)) {
                    BarView categoryBar = (BarView) child.findViewById(categoriesToBarIds.get(categoryData.categoryName));
                    TextView categoryScoreTextView = (TextView) child.findViewById(categoriesToScoreTextIds.get(categoryData.categoryName));

                    categoryBar.setColor(Color.getColorBaseline(getContext(), categoryData.mean, categoryData.baseline));
                    categoryBar.setFractionToFill((categoryData.mean - 1F) / 4F);
                    categoryScoreTextView.setText(String.valueOf((Math.round(categoryData.mean * 10.0f))/10.0f));

                    animateWhenVisible(categoryBar);
                }
            }
            linearLayout.addView(child);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        TermData term = courseData.get(selectedTerm);
        Intent intent = new Intent(getActivity(), DetailedBreakdownActivity.class);

        switch (v.getId()) {
            case R.id.overall_scoreview:
                intent.putExtra(BREAKDOWN_TYPE, "Overall Score Details");
                intent.putExtra(BREAKDOWN_QUESTION, "Evaluate the course overall.");
                intent.putExtra(BREAKDOWN_DATA, term.getOverallBreakdown());

                String[] overallLabels = {"bad", "fair", "good", "great", "excellent"};
                intent.putExtra(BREAKDOWN_LABELS, overallLabels);

                startActivity(intent);
                break;
            case R.id.workload_scoreview:
                intent.putExtra(BREAKDOWN_TYPE, "Workload Score Details");
                intent.putExtra(BREAKDOWN_QUESTION, "On average, how many hours per week did you spend on coursework outside of class?");
                intent.putExtra(BREAKDOWN_DATA, term.getWorkloadBreakdown());

                String[] workloadLabels = {"< 3", "3 - 6", "7 - 10", "11 - 14", " > 14"};
                intent.putExtra(BREAKDOWN_LABELS, workloadLabels);

                startActivity(intent);
                break;
        }
    }
}