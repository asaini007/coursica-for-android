package com.coursica.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.coursica.coursica.R;
import com.coursica.helpers.FirebaseHelper;
import com.coursica.model.Course;
import com.coursica.model.Meeting;
import com.coursica.model.Offering;
import com.firebase.client.Firebase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;

public class SummaryFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "SummaryFragment";
    String[] days = {"Su", "M", "Tu", "W", "Th", "F", "Sa"};
    private Course course;

    public static SummaryFragment newInstance(String number, String shortField, String title) {
        Bundle args = new Bundle();
        args.putString("number", number);
        args.putString("shortField", shortField);
        args.putString("title", title);
        SummaryFragment fragment = new SummaryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String number = getArguments().getString("number");
        String shortField = getArguments().getString("shortField");
        String title = getArguments().getString("title");

        Realm realm;
        try {
            realm = Realm.getDefaultInstance();
        } catch(NullPointerException e) {
            Firebase.setAndroidContext(getContext());
            RealmConfiguration realmConfig = new RealmConfiguration.Builder(getContext())
                    .name("courses.realm")
                    .build();
            Realm.setDefaultConfiguration(realmConfig);
            realm = Realm.getDefaultInstance();
        }
        this.course = realm.where(Course.class)
                .equalTo("number", number)
                .equalTo("shortField", shortField)
                .equalTo("title", title)
                .findFirst();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_summary, container, false);

        // title
        TextView titleView = (TextView) mainView.findViewById(R.id.courseTitle);
        titleView.setText(course.getTitle());

        // description
        TextView descriptionView = (TextView) mainView.findViewById(R.id.courseDescription);
        descriptionView.setText(course.getCourseDescription());

        // faculty
        setText((TextView) mainView.findViewById(R.id.courseInstructor),
                course.getFaculty(), true,
                "getFirst", "getLast");

        int latestId = setUpEnrollment(mainView);
        Map<Course, String> conflicts = setUpOfferings(mainView, latestId);
        latestId = setUpConflicts(mainView, conflicts);
        latestId = setUpGenEds(mainView, latestId);
        latestId = setUpPrereqs(mainView, latestId);
        latestId = setUpNotes(mainView, latestId);

        // shopping buttons
        Button addButton = (Button) mainView.findViewById(R.id.add_button);
        addButton.setOnClickListener(this);

        Button removeButton = (Button) mainView.findViewById(R.id.remove_button);
        removeButton.setOnClickListener(this);

        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
        Set<String> shoppingNumbers = prefs.getStringSet(getString(R.string.pref_shopping), new HashSet<String>());
        if (shoppingNumbers.contains(course.getCatalogNumber())) {
            removeButton.setVisibility(View.VISIBLE);
        } else {
            addButton.setVisibility(View.VISIBLE);
        }

        return mainView;
    }

    private int setUpNotes(View mainView, int latestId) {
        TextView notes = (TextView) mainView.findViewById(R.id.courseNotes);
        TextView notesText = (TextView) mainView.findViewById(R.id.notes_text);
        if(course.getNotes().length() > 0) {
            notes.setText(course.getNotes());
            setBelow(latestId, notes, notesText);
            return R.id.courseNotes;
        } else {
            setGone(notes, notesText);
            return latestId;
        }
    }

    private int setUpPrereqs(View mainView, int latestId) {
        TextView prereqs = (TextView) mainView.findViewById(R.id.coursePrerequisites);
        TextView prereqsText = (TextView) mainView.findViewById(R.id.prerequisite_text);
        if(course.getPrerequisitesString().length() > 0) {
            prereqs.setText(course.getPrerequisitesString());
            setBelow(latestId, prereqs, prereqsText);
            return R.id.coursePrerequisites;
        } else {
            setGone(prereqs, prereqsText);
            return latestId;
        }
    }

    private int setUpGenEds(View mainView, int latestId) {
        TextView genEds = (TextView) mainView.findViewById(R.id.courseGenEd);
        TextView genEdsText = (TextView) mainView.findViewById(R.id.gened_text);
        if(course.getGenEds().size() > 0) {
            setText(genEds, course.getGenEds(), false, "getName");
            setBelow(latestId, genEds, genEdsText);
            return R.id.courseGenEd;
        } else {
            setGone(genEds, genEdsText);
            return latestId;
        }
    }

    private void setGone(View... views) {
        for(View view : views) {
            view.setVisibility(View.GONE);
        }
    }

    private void setBelow(int latestId, View... views) {
        RelativeLayout.LayoutParams params;
        for(View view : views) {
            params = (RelativeLayout.LayoutParams) view.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, latestId);
            view.setLayoutParams(params);
        }
    }

    private int setUpConflicts(View mainView, Map<Course, String> conflicts) {
        if(conflicts.size() > 0) {
            String conflictsString = "";
            for(Course conflictCourse : conflicts.keySet()) {
                String courseString = conflictsString.isEmpty() ? "" : "<br>";
                courseString += "<font color=\"#FF1F1F\"><b>";
                courseString += conflictCourse.getShortField() + " " + conflictCourse.getNumber();
                courseString += "</b></font>" ;

                String termAndYear = conflicts.get(conflictCourse);
                String term = termAndYear.split(" ")[0];
                String year = termAndYear.split(" ")[1];
                courseString +="<font color=\"#9E9E9E\"><i>";
                String meetingsString = "";
                List<Offering> conflictOfferings = new ArrayList<>(conflictCourse.getOfferings());
                for(Offering conflictOffering : conflictOfferings) {
                    if(conflictOffering.getTerm().equals(term) && conflictOffering.getYear().equals(year)) {
                        String offeringString = "";

                        List<Meeting> meetings = new ArrayList<>(conflictOffering.getMeetings());
                        while (meetings.size() > 0) {
                            List<Meeting> commonMeetings = getCommonMeetings(meetings);
                            String commonMeetingString = getMeetingStringSimple(commonMeetings);
                            String separator = offeringString.equals("") ? " " : ", ";
                            offeringString += separator + commonMeetingString;
                        }

                        String separator = meetingsString.equals("") ? "" : "; ";
                        meetingsString += separator + offeringString;
                    }
                }
                courseString += meetingsString;
                courseString += "</i></font>";
                conflictsString += courseString;
            }
            TextView courseConflicts = (TextView) mainView.findViewById(R.id.courseConflicts);
            courseConflicts.setText(Html.fromHtml(conflictsString));
            return R.id.courseConflicts;
        } else {
            mainView.findViewById(R.id.conflicts_text).setVisibility(View.GONE);
            mainView.findViewById(R.id.courseConflicts).setVisibility(View.GONE);
            return R.id.courseTime;
        }
    }

    private Map<Course, String> setUpOfferings(View mainView, int latestId) {
        Map<Course, String> conflicts = new HashMap<>();
        List<Offering> offerings = new ArrayList<>(course.getOfferings());
        if(offerings.size() > 0) {
            TextView meetingsView = (TextView) mainView.findViewById(R.id.courseTime);
            String meetsString = "";
            for(Offering offering : offerings) {
                String offeringString = "";

                List<Meeting> meetings = new ArrayList<>(offering.getMeetings());
                while(meetings.size() > 0) {
                    List<Meeting> commonMeetings = getCommonMeetings(meetings);
                    String commonMeetingString = getMeetingsString(commonMeetings,
                            offering.getTerm(), offering.getYear(), conflicts);
                    String separator = offeringString.equals("") ? " " : "; ";
                    offeringString += separator + commonMeetingString;
                }

                if(!offering.getLocation().isEmpty()) {
                    if(offering.getLatitude().isEmpty() || offering.getLongitude().isEmpty()) {
                        offeringString += " in <b>" + offering.getLocation() + "</b>";
                    } else {
                        offeringString += " in <a href='com.coursica.map://location/"
                                + offering.getLatitude() + "/"
                                + offering.getLongitude() + "/"
                                + offering.getLocation() + "'>"
                                + "<b>" + offering.getLocation() + "</b></a>";
                    }
                }

                String semester = "<b>" + offering.getTerm() + " '" + offering.getYear().substring(2) + "</b>";
                offeringString = semester + offeringString;
                meetsString += meetsString.isEmpty() ? offeringString : "<br>" + offeringString;
            }
            meetingsView.setText(Html.fromHtml(meetsString));
            stripUnderlines(meetingsView);
            meetingsView.setMovementMethod(LinkMovementMethod.getInstance());

            setBelow(latestId, meetingsView, mainView.findViewById(R.id.meets_text));
        }
        return conflicts;
    }

    private int setUpEnrollment(View mainView) {
        TextView enrollmentText = (TextView) mainView.findViewById(R.id.courseEnrollment);
        TextView enrollmentSource = (TextView) mainView.findViewById(R.id.courseEnrollmentSource);
        int enrollment = course.getEnrollment();
        if(enrollment > 0) {
            enrollmentText.setText(String.valueOf(enrollment));
            String source = parseEnrollmentSource(course.getEnrollmentSource());
            enrollmentSource.setText(source);
            return R.id.courseEnrollment;
        } else {
            setGone(mainView.findViewById(R.id.enrollment_text), enrollmentText,
                    mainView.findViewById(R.id.buffer_space), enrollmentSource);
            return R.id.courseInstructor;
        }
    }

    private String parseEnrollmentSource(String enrollmentSource) {
        if(enrollmentSource != null) {
            if(enrollmentSource.startsWith("fall")) {
                return "(Fall " + enrollmentSource.substring(4) + ")";
            } else {
                return "(Spring " + enrollmentSource.substring(6) + ")";
            }
        } else {
            return "";
        }
    }

    private List<Meeting> getCommonMeetings(List<Meeting> offeringMeetings) {
        String beginTime = null;
        String endTime = null;
        List<Meeting> meetings = new ArrayList<>();
        for (int i = 0; i < offeringMeetings.size(); i++) {
            Meeting m = offeringMeetings.get(i);
            if (beginTime == null) {
                beginTime = m.getBeginTime();
                endTime = m.getEndTime();
                meetings.add(m);
                offeringMeetings.remove(i);
                i--;
            } else if (m.getBeginTime().equals(beginTime) && m.getEndTime().equals(endTime)) {
                meetings.add(m);
                offeringMeetings.remove(i);
                i--;
            }
        }

        Collections.sort(meetings, new Comparator<Meeting>() {
            @Override
            public int compare(Meeting lhs, Meeting rhs) {
                return Integer.parseInt(lhs.getDay()) - Integer.parseInt(rhs.getDay());
            }
        });

        return meetings;
    }

    private String getMeetingStringSimple(List<Meeting> meetings) {
        String meetingsString = "";
        for (int i = 0; i < meetings.size() - 1; i++) {
            meetingsString += days[Integer.parseInt(meetings.get(i).getDay())] + ", ";
        }
        meetingsString += days[Integer.parseInt(meetings.get(meetings.size() - 1).getDay())];
        meetingsString += " from " + parseTime(meetings.get(0).getBeginTime());
        meetingsString += "-" + parseTime(meetings.get(0).getEndTime());
        return meetingsString;
    }

    private String getMeetingsString(List<Meeting> meetings, String term, String year, Map<Course, String> conflicts) {
        String meetingsString = getMeetingStringSimple(meetings);

        // check for conflicts
        String color = "#1EBE38";
        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
        Set<String> shoppingNumbers = prefs.getStringSet(getString(R.string.pref_shopping), new HashSet<String>());
        Realm realm = Realm.getDefaultInstance();
        for(String catalogNumber : shoppingNumbers) {
            Course shoppingCourse = realm.where(Course.class).equalTo("catalogNumber", catalogNumber).findFirst();
            if (!shoppingCourse.getCatalogNumber().equals(course.getCatalogNumber())) {
                boolean conflict = false;
                List<Offering> shoppingCourseOfferings = new ArrayList<>(shoppingCourse.getOfferings());
                for (Offering shoppingCourseOffering : shoppingCourseOfferings) {
                    if (shoppingCourseOffering.getTerm().equals(term) && shoppingCourseOffering.getYear().equals(year)) {
                        if (isOfferingConflict(shoppingCourseOffering, meetings)) {
                            conflict = true;
                        } else {
                            conflict = false;
                            break;
                        }
                    }
                }
                if (conflict) {
                    color = "#FF1F1F";
                    conflicts.put(shoppingCourse, term + " " + year);
                }
            }
        }

        return "<font color=\"" + color + "\"><b>" + meetingsString + "</b></font>";
    }

    private boolean isOfferingConflict(Offering offering, List<Meeting> meetings) {
        List<Meeting> otherMeetings = new ArrayList<>(offering.getMeetings());
        for (Meeting otherMeeting : otherMeetings) {
            for (Meeting meeting : meetings) {
                if (meetingsConflict(otherMeeting, meeting)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean meetingsConflict(Meeting meeting1, Meeting meeting2) {
        if(!meeting1.getDay().equals(meeting2.getDay())) {
            return false;
        }
        String[] startTime1 = meeting1.getBeginTime().split("[^0-9]");
        String[] endTime1 = meeting1.getEndTime().split("[^0-9]");
        String[] startTime2 = meeting2.getBeginTime().split("[^0-9]");
        String[] endTime2 = meeting2.getEndTime().split("[^0-9]");
        if(startTime1.length == 2 && startTime2.length == 2
            && endTime1.length == 2 && endTime2.length == 2) {
            if(before(startTime1, startTime2)) {
                if(after(endTime1, startTime2)) {
                    return true;
                }
            } else if(after(startTime1, startTime2)) {
                if(before(startTime1, endTime2)) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    private boolean before(String[] time1, String[] time2) {
        int hour1 = Integer.parseInt(time1[0]);
        int minute1 = Integer.parseInt(time1[1]);
        int hour2 = Integer.parseInt(time2[0]);
        int minute2 = Integer.parseInt(time2[1]);
        if(hour1 < hour2 || (hour1 == hour2 && minute1 < minute2)) {
            return true;
        } else if(hour2 < hour1 || (hour1 == hour2 && minute2 < minute1)) {
            return false;
        } else {
            return false;
        }
    }

    private boolean after(String[] time1, String[] time2) {
        int hour1 = Integer.parseInt(time1[0]);
        int minute1 = Integer.parseInt(time1[1]);
        int hour2 = Integer.parseInt(time2[0]);
        int minute2 = Integer.parseInt(time2[1]);
        if(hour1 > hour2 || (hour1 == hour2 && minute1 > minute2)) {
            return true;
        } else if(hour2 > hour1 || (hour1 == hour2 && minute2 > minute1)) {
            return false;
        } else {
            return false;
        }
    }

    private void setText(TextView textView, RealmList<? extends RealmObject> objects, boolean link, String... methods) {
        if(objects.isEmpty()) {
            return;
        }

        String[] textArray = new String[objects.size()];
        Class<? extends RealmObject> c = objects.get(0).getClass();
        Method[] allMethods = c.getDeclaredMethods();
        for(int i = 0; i < objects.size(); i++) {
            String linkString = "<a href='com.coursica.faculty://faculty/";
            String[] subTextArray = new String[methods.length];
            RealmObject obj = objects.get(i);
            for (int j = 0; j < methods.length; j++) {
                String methodName = methods[j];
                for (Method m : allMethods) {
                    if (m.getName().equals(methodName)) {
                        try {
                            String token = (String) m.invoke(obj);
                            subTextArray[j] = token;
                            if(link) {
                                linkString += token + "/";
                            }
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            textArray[i] = insertDelimiter(subTextArray, " ");
            if(link) {
                textArray[i] = linkString + "'>" + textArray[i] + "</a>";
            }
        }
        textView.setText(Html.fromHtml(insertDelimiter(textArray, ", ")));
        if(link) {
            stripUnderlines(textView);
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void stripUnderlines(TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    private String insertDelimiter(String[] strings, String delimiter) {
        String toReturn = "";
        for(int i = 0; i < strings.length - 1; i++) {
            toReturn += strings[i] + delimiter;
        }
        toReturn += strings[strings.length - 1];
        return toReturn;
    }

    private String parseTime(String time) {
        String toReturn = "";
        String[] timeArray = time.split("[^0-9]");
        if(timeArray.length == 2) {
            int hour = Integer.parseInt(timeArray[0]);
            if (hour > 12) {
                hour -= 12;
            }
            toReturn += hour;
            int minute = Integer.parseInt(timeArray[1]);
            if (minute > 0) {
                toReturn += ":" + minute;
            }
            return toReturn;
        } else {
            return time;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_button: addToShopping();
                break;
            case R.id.remove_button: removeFromShopping();
        }
    }

    private void addToShopping() {
        final View mainView = getView();
        final Button addButton = (Button) mainView.findViewById(R.id.add_button);
        final Button removeButton = (Button) mainView.findViewById(R.id.remove_button);

        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
        Set<String> shoppingNumbers = new HashSet<>();
        shoppingNumbers.addAll(prefs.getStringSet(getString(R.string.pref_shopping), new HashSet<String>()));
        shoppingNumbers.add(course.getCatalogNumber());
        prefs.edit().putStringSet(getString(R.string.pref_shopping), shoppingNumbers).apply();

        FirebaseHelper.addCourseToShopping(getContext(), course);

        new CountDownTimer(150, 100) {
            public void onFinish() {
                addButton.setVisibility(View.GONE);
                removeButton.setVisibility(View.VISIBLE);
            }

            public void onTick(long millisUntilFinished) {
            }
        }.start();
    }

    private void removeFromShopping() {
        final View mainView = getView();
        final Button addButton = (Button) mainView.findViewById(R.id.add_button);
        final Button removeButton = (Button) mainView.findViewById(R.id.remove_button);

        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
        Set<String> shoppingNumbers = new HashSet<>();
        shoppingNumbers.addAll(prefs.getStringSet(getString(R.string.pref_shopping), new HashSet<String>()));
        shoppingNumbers.remove(course.getCatalogNumber());
        prefs.edit().putStringSet(getString(R.string.pref_shopping), shoppingNumbers).apply();

        FirebaseHelper.removeCourseFromShopping(getContext(), course);

        new CountDownTimer(150, 100) {
            public void onFinish() {
                addButton.setVisibility(View.VISIBLE);
                removeButton.setVisibility(View.GONE);
            }

            public void onTick(long millisUntilFinished) {
            }
        }.start();
    }

    private class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }
}
