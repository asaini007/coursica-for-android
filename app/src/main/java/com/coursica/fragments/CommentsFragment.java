package com.coursica.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.coursica.adapters.CommentListAdapter;
import com.coursica.coursica.R;
import com.coursica.helpers.FirebaseHelper;
import com.coursica.model.Course;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class CommentsFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    DataSnapshot courseSnapshot = null;
    ArrayAdapter<String> termsAdapter;
    ArrayList<String> terms;
    ArrayList<String> comments;
    BaseAdapter adapter;
    String year = null;
    String term = null;
    Course course;

    public static CommentsFragment newInstance(String number, String shortField, String title) {
        Bundle args = new Bundle();
        args.putString("number", number);
        args.putString("shortField", shortField);
        args.putString("title", title);
        CommentsFragment fragment = new CommentsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String number = getArguments().getString("number");
        String shortField = getArguments().getString("shortField");
        String title = getArguments().getString("title");

        terms = new ArrayList<>();
        comments = new ArrayList<>();
        termsAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item_term, terms);

        Realm realm;
        try {
            realm = Realm.getDefaultInstance();
        } catch(NullPointerException e) {
            Firebase.setAndroidContext(getContext());
            RealmConfiguration realmConfig = new RealmConfiguration.Builder(getContext())
                    .name("courses.realm")
                    .build();
            Realm.setDefaultConfiguration(realmConfig);
            realm = Realm.getDefaultInstance();
        }
        this.course = realm.where(Course.class)
                .equalTo("number", number)
                .equalTo("shortField", shortField)
                .equalTo("title", title)
                .findFirst();

        final String childName = FirebaseHelper.courseChildName(shortField, number);
        Firebase fb = new Firebase("https://glaring-heat-9505.firebaseio.com/").child(childName);
        fb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                initialize(dataSnapshot);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void initialize(DataSnapshot dataSnapshot) {
        courseSnapshot = dataSnapshot;
        for (DataSnapshot termSnapshot : dataSnapshot.getChildren()) {
            if (termSnapshot.hasChild("year") && termSnapshot.hasChild("term")
                    && termSnapshot.hasChild("comments")) {
                String year = (String) termSnapshot.child("year").getValue();
                String term = (String) termSnapshot.child("term").getValue();
                terms.add(term.substring(0, 1).toUpperCase() + term.substring(1) + " " + year);
            }
        }
        CommentsFragment.this.termsAdapter.notifyDataSetChanged();
        refresh();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(courseSnapshot == null) {
            return inflater.inflate(R.layout.loading, container, false);
        } else if(!courseSnapshot.exists()) {
            View mainView = inflater.inflate(R.layout.text_view_course, container, false);
            ((TextView) mainView.findViewById(R.id.centered_text_view)).setText("No data found.");
            return mainView;
        } else {
            View mainView = inflater.inflate(R.layout.fragment_comments, container, false);

            Spinner spinner = (Spinner) mainView.findViewById(R.id.comments_course_spinner);
            spinner.setAdapter(termsAdapter);
            spinner.setOnItemSelectedListener(this);

            adapter = new CommentListAdapter(getActivity(), comments);
            ListView listView = (ListView) mainView.findViewById(R.id.comment_list_view);
            listView.setAdapter(adapter);

            TextView commentCount = (TextView) mainView.findViewById(R.id.comment_count);
            commentCount.setText(comments.size() + " comments");

            return mainView;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = (String) parent.getItemAtPosition(position);
        String[] tokens = item.split(" ");
        final String term = tokens[0].toLowerCase();
        final String year = tokens[1];

        if(this.year != null && this.year.equals(year) && this.term.equals(term)) {
            return;
        }
        this.year = year;
        this.term = term;
        for (DataSnapshot termSnapshot : courseSnapshot.getChildren()) {
            if (termSnapshot.hasChild("year") && termSnapshot.hasChild("term")
                    && termSnapshot.hasChild("comments")) {
                String fbYear = (String) termSnapshot.child("year").getValue();
                String fbTerm = (String) termSnapshot.child("term").getValue();
                if (fbYear.equals(year) && fbTerm.equals(term)) {
                    CommentsFragment.this.comments = (ArrayList<String>) termSnapshot.child("comments").getValue();
                    CommentsFragment.this.adapter.notifyDataSetChanged();
                }
            }
        }
        refresh();
    }

    private void refresh() {
        android.support.v4.app.FragmentManager fm = getFragmentManager();
        if(fm != null) {
            Fragment currentFragment = getFragmentManager().findFragmentByTag(getTag());
            FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
            fragTransaction.detach(currentFragment);
            fragTransaction.attach(currentFragment);
            fragTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
