package com.coursica.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.PathInterpolator;

import com.coursica.coursica.R;
import com.coursica.helpers.Animatable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HistogramView extends Animatable implements ValueAnimator.AnimatorUpdateListener {
    private static final String TAG = "HistogramView";
    private static final float DURATION = 1000;
    private static final int BAR_WIDTH = 9;
    private static final int BAR_SPACING = 6;

    List<Float> data;
    float selectedValue;
    int selectedColor, selectedBucket, numBars;
    private boolean animate = false, animated = false;

    private boolean hasData = false, hasLayout = false;

    AnimatorSet animations = null;

    float leftOffset;
    int pixelWidth;
    int pixelSpacing;
    Paint linePaint, pointPaint, selectedLinePaint, selectedPointPoint;

    public HistogramView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Log.d(TAG, "new HistogramView constructed");

        selectedLinePaint = new Paint();
        selectedPointPoint = new Paint();
    }

    public void set(List<Float> data, float selected, int color) {
        if(data != null && data.size() > 0) {
            this.data = data;
            this.selectedValue = selected;
            this.selectedColor = color;

            selectedLinePaint.setColor(selectedColor);
            selectedPointPoint.setColor(selectedColor);

            hasData = true;

            if(hasLayout) {
                setUpAnimation();
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int width = w - getPaddingLeft() - getPaddingRight();

        pixelWidth = (int) getResources().getDisplayMetrics().density * BAR_WIDTH;
        pixelSpacing = (int) getResources().getDisplayMetrics().density * BAR_SPACING;

        numBars = 1 + ((width - pixelWidth) / (pixelWidth + pixelSpacing)) ;

        linePaint = new Paint();
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setColor(ContextCompat.getColor(getContext(), R.color.light_gray));
        linePaint.setStrokeWidth(pixelWidth);
        linePaint.setAntiAlias(true);

        pointPaint = new Paint();
        pointPaint.setStyle(Paint.Style.FILL);
        pointPaint.setColor(ContextCompat.getColor(getContext(), R.color.light_gray));
        pointPaint.setStrokeWidth(pixelWidth);
        pointPaint.setAntiAlias(true);

        selectedLinePaint.setStyle(Paint.Style.STROKE);
        selectedLinePaint.setStrokeWidth(pixelWidth);
        selectedLinePaint.setAntiAlias(true);

        selectedPointPoint.setStyle(Paint.Style.FILL);
        selectedPointPoint.setStrokeWidth(pixelWidth);
        selectedPointPoint.setAntiAlias(true);

        leftOffset = getPaddingLeft() + ((width - pixelWidth - (numBars - 1) * (pixelWidth + pixelSpacing)) / 2);

        hasLayout = true;

        if(hasData) {
            setUpAnimation();
        }
    }

    private void setUpAnimation() {
        Collections.sort(data);

        float lowestScore = data.get(0);
        float highestScore = data.get(data.size() - 1);
        float bucketSize = (highestScore - lowestScore) / numBars;

        float[] buckets = new float[numBars];
        for(int i = 1; i <= numBars; i++) {
            buckets[i-1] = lowestScore + bucketSize * i;
        }

        selectedBucket = 0;
        for(int i = 1; i <= buckets.length; i++) {
            if(selectedValue > buckets[i - 1]) {
                selectedBucket++;
            } else {
                break;
            }
        }

        float[] fractions = new float[numBars];
        for(int i = 0; i < buckets.length; i++) {
            float max = buckets[i];
            int count = 0;
            while(count < data.size() && data.get(count) <= max) {
                count++;
            }
            fractions[i] = ((float) count) / ((float) data.size());
        }

        int height = getHeight() - getPaddingTop() - getPaddingBottom() - pixelWidth;
        List<Animator> animators = new ArrayList<>();
        for (int i = 0; i < fractions.length; i++) {
            ValueAnimator animator = ValueAnimator.ofFloat(0, fractions[i] * height);
            animator.setDuration((long) DURATION);
            animator.setStartDelay(i * 50);
            animator.addUpdateListener(this);
            animators.add(animator);
        }
        animations = new AnimatorSet();
        animations.playTogether(animators);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            animations.setInterpolator(new PathInterpolator(0.25F, 0.1F, 0.25F, 1F));
        } else {
            animations.setInterpolator(new AccelerateDecelerateInterpolator());
        }

        if(animate && !animated) {
            animations.start();
            animated = true;
        }
    }

    public void runAnimation() {
        animate = true;
        if(hasData && hasLayout && animations != null) {
            animated = true;
            animations.start();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(hasData && hasLayout) {
            List<Animator> animators = animations.getChildAnimations();

            float startY = getHeight() - getPaddingBottom() - (pixelWidth / 2F);
            float x, stopY;
            for (int i = 0; i < animators.size(); i++) {
                ValueAnimator animator = (ValueAnimator) animators.get(i);

                x = leftOffset + i * (pixelWidth + pixelSpacing) + (pixelWidth / 2);
                stopY = startY - (Float) animator.getAnimatedValue();
                if(i == selectedBucket) {
                    canvas.drawLine(x, startY, x, stopY, selectedLinePaint);
                    canvas.drawCircle(x, startY, pixelWidth / 2F, selectedPointPoint);
                    canvas.drawCircle(x, stopY, pixelWidth / 2F, selectedPointPoint);
                } else {
                    canvas.drawLine(x, startY, x, stopY, linePaint);
                    canvas.drawCircle(x, startY, pixelWidth / 2F, pointPaint);
                    canvas.drawCircle(x, stopY, pixelWidth / 2F, pointPaint);
                }
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int defaultHeight = (int) getResources().getDisplayMetrics().density * 130;
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height;

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(defaultHeight, heightSize);
        } else {
            height = defaultHeight;
        }

        setMeasuredDimension(width, height);
    }

    public boolean hasData() {
        return hasData;
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        invalidate();
    }
}
