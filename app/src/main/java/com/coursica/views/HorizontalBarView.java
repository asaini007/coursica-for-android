package com.coursica.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import static com.coursica.views.BarView.Label.NONE;

public class HorizontalBarView extends BarView {
    private static final String TAG = "HorizontalBarView";

    private static final float TEXT_SCALE = 0.6F;
    private static final float TEXT_OFFSET = 0.5F;

    private float left, top, bottom, circleRadius, totalWidth;

    public HorizontalBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // called when layout is changed
    protected void recalculateDimensions() {
        top = getPaddingTop();
        bottom = getHeight() - getPaddingBottom();
        float height = bottom - top;
        circleRadius = height / 2;

        if(labelType != NONE && textSize < 0) {
            textPaint.setTextSize(100);
            String text = getFinalLabel();
            textPaint.getTextBounds(text, 0, text.length(), textBounds);

            float textSize = TEXT_SCALE * textPaint.getTextSize() * height / textBounds.height();
            textPaint.setTextSize(textSize);
            textPaint.getTextBounds(text, 0, text.length(), textBounds);
        }

        left = getPaddingLeft() + circleRadius;
        float endRight = getWidth() - getPaddingRight() - circleRadius;

        if(labelType != NONE) {
            endRight = endRight - textBounds.width() - TEXT_OFFSET * circleRadius;
        }

        totalWidth = endRight - left;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float right = left + fractionFilled * totalWidth;
        float centerY = top + circleRadius;

        canvas.drawCircle(left, centerY, circleRadius, paint);
        canvas.drawRect(left, top, right, bottom, paint);
        canvas.drawCircle(right, centerY, circleRadius, paint);
        if(labelType != NONE) {
            String label = getLabel();
            float textCenterX = right + circleRadius + TEXT_OFFSET * circleRadius + textBounds.width() / 2F - textBounds.left;
            float textBaselineY = centerY + textBounds.height() / 2F - textBounds.bottom;
            canvas.drawText(label, textCenterX, textBaselineY, textPaint);
        }
    }
}
