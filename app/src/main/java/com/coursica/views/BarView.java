package com.coursica.views;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.PathInterpolator;

import com.coursica.coursica.R;
import com.coursica.helpers.Animatable;

import static com.coursica.views.BarView.Label.DECIMAL;
import static com.coursica.views.BarView.Label.INTEGER;
import static com.coursica.views.BarView.Label.NONE;

public abstract class BarView extends Animatable implements ValueAnimator.AnimatorUpdateListener {
    protected enum Label { INTEGER, DECIMAL, NONE }

    protected int textSize;

    protected Label labelType;
    float finalValue;

    protected Paint paint, textPaint;
    protected Rect textBounds;

    ObjectAnimator animator;
    protected float fractionToFill, fractionFilled;

    public BarView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.BarView,
                0, 0);

        int color;
        try {
            color = a.getColor(R.styleable.BarView_barColor, getResources().getColor(R.color.primary));
            textSize = a.getDimensionPixelSize(R.styleable.BarView_labelTextSize, -1);
        } finally {
            a.recycle();
        }

        labelType = NONE;

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color);
        paint.setAntiAlias(true);
    }

    public void setFractionFilled(float fractionFilled) {
        this.fractionFilled = fractionFilled;
    }

    public void setColor(int color) {
        paint.setColor(color);
    }

    public void setFractionToFill(float fractionToFill) {
        this.fractionToFill = fractionToFill;
        animator = ObjectAnimator.ofFloat(this, "fractionFilled", 0F, fractionToFill);
        animator.setDuration(2000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            animator.setInterpolator(new PathInterpolator(0.25F, 0.1F, 0.25F, 1F));
        } else {
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
        }
        animator.addUpdateListener(this);
    }

    public void setLabel(int finalValue) {
        labelType = INTEGER;
        this.finalValue = finalValue;

        initializeTextPaint();
        recalculateDimensions();
    }

    public void setLabel(float finalValue) {
        labelType = DECIMAL;
        this.finalValue = finalValue;

        initializeTextPaint();
        recalculateDimensions();
    }

    private void initializeTextPaint() {
        textBounds = new Rect();
        textPaint = new TextPaint();
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(0xff000000);
        textPaint.setTextSize(100F);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.CENTER);

        if(textSize > 0) {
            textPaint.setTextSize(textSize);
            String text = getFinalLabel();
            textPaint.getTextBounds(text, 0, text.length(), textBounds);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        recalculateDimensions();
    }

    protected abstract void recalculateDimensions();

    @Override
    protected abstract void onDraw(Canvas canvas);

    protected String getLabel() {
        float currentValue = (fractionFilled / fractionToFill) * finalValue;
        switch (labelType) {
            case INTEGER:
                return String.valueOf(Math.round(currentValue));
            case DECIMAL:
                return String.valueOf(Math.round(currentValue * 10F) / 10F);
            case NONE:
                return "Error";
        }
        return "Error";
    }

    protected String getFinalLabel() {
        return (labelType == INTEGER) ?  Integer.toString((int) finalValue) : Float.toString(finalValue);
    }

    public void runAnimation() {
        animator.start();
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        invalidate();
    }
}
