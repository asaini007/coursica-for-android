package com.coursica.views;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.coursica.coursica.R;

import java.util.ArrayList;
import java.util.List;

public class SliderView extends View implements ValueAnimator.AnimatorUpdateListener {
    private static final String TAG = "com.coursica.SliderView";
    private enum Circle {LEFT, RIGHT, NEITHER}
    private static final float LINE_RADIUS_DP = 1.75f;
    private static final float CIRCLE_RADIUS_DP = 6.5f;

    private List<OnChangeListener> changeListeners = new ArrayList<>();

    private int primaryColor, secondaryColor, maxBucket;
    private Paint primaryPaint, secondaryPaint;
    private float effectiveWidth, yCenter, leftCircleRadius, rightCircleRadius,
            defaultCircleRadius, leftCircleX, rightCircleX, leftPadding, rightPadding;
    private RectF secondaryLine, primaryLine;
    private Circle selected = Circle.NEITHER;

    public SliderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SliderView, 0, 0);
        try {
            primaryColor = a.getColor(R.styleable.SliderView_primaryColor, getResources().getColor(R.color.primary));
            secondaryColor = a.getColor(R.styleable.SliderView_secondaryColor, Color.LTGRAY);
            maxBucket = a.getInt(R.styleable.SliderView_maxBucket, 40);
        } finally {
            a.recycle();
        }

        initializeDrawingTools();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (x < leftCircleX) {
                    select(Circle.LEFT);
                    moveSelected(event.getX());
                } else if (x > rightCircleX) {
                    select(Circle.RIGHT);
                    moveSelected(event.getX());
                } else {
                    if(leftCircleX + 3f * defaultCircleRadius > rightCircleX - 3f * defaultCircleRadius) {
                        if(x - leftCircleX < rightCircleX - x) {
                            select(Circle.LEFT);
                            moveSelected(event.getX());
                        } else {
                            select(Circle.RIGHT);
                            moveSelected(event.getX());
                        }
                    } else if(x < leftCircleX + 3f * defaultCircleRadius) {
                        select(Circle.LEFT);
                        moveSelected(event.getX());
                    } else if (x > rightCircleX - 3f * defaultCircleRadius) {
                        select(Circle.RIGHT);
                        moveSelected(event.getX());
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                unselect();
                break;
            case MotionEvent.ACTION_MOVE:
                moveSelected(event.getX());
                break;
        }
        return true;
    }

    // unselect any selected circle, then enlarge the given one
    private void select(Circle circle) {
        unselect();
        if(circle != Circle.NEITHER) {
            selected = circle;
            String fieldName = (circle == Circle.LEFT) ? "leftCircleRadius" : "rightCircleRadius";
            ObjectAnimator animator = ObjectAnimator.ofFloat(this, fieldName, 1.5f * defaultCircleRadius);
            animator.setDuration(50);
            animator.addUpdateListener(this);
            if(Build.VERSION.SDK_INT >= 18) {
                animator.setAutoCancel(true);
            }
            animator.start();
        }
    }

    // if a circle is selected, lock it to nearest bucket and shrink it
    private void unselect() {
        if(selected != Circle.NEITHER) {
            if(selected == Circle.LEFT) {
                leftCircleX = getXFromBucket(getNearestBucket(leftCircleX));
                notifyMinChange(true);
            } else {
                rightCircleX = getXFromBucket(getNearestBucket(rightCircleX));
                notifyMaxChange(true);
            }
            String fieldName = (selected == Circle.LEFT) ? "leftCircleRadius" : "rightCircleRadius";
            ObjectAnimator animator = ObjectAnimator.ofFloat(this, fieldName, defaultCircleRadius);
            animator.setDuration(50);
            animator.addUpdateListener(this);
            if(Build.VERSION.SDK_INT >= 18) {
                animator.setAutoCancel(true);
            }
            animator.start();
        }
        selected = Circle.NEITHER;
    }

    private float getXFromBucket(int bucket) {
        return leftPadding +  effectiveWidth * ((float) bucket / (float) maxBucket);
    }

    private void moveSelected(float x) {
        if(selected == Circle.LEFT) {
            if(x < rightCircleX) {
                leftCircleX = (x < leftPadding) ? leftPadding : x;
                notifyMinChange(false);
            } else {
                leftCircleX = rightCircleX;
                select(Circle.RIGHT);
                rightCircleX = (x > getWidth() - rightPadding) ? getWidth() - rightPadding : x;
                notifyMaxChange(false);
            }
        } else if(selected == Circle.RIGHT) {
            if(x > leftCircleX) {
                rightCircleX = (x > getWidth() - rightPadding) ? getWidth() - rightPadding : x;
                notifyMaxChange(false);
            } else {
                rightCircleX = leftCircleX;
                select(Circle.LEFT);
                leftCircleX = (x < leftPadding) ? leftPadding : x;
                notifyMinChange(false);
            }
        }
        invalidate();
    }

    private int getNearestBucket(float x) {
        if(x < leftPadding) {
            return 0;
        } else if(x > getWidth() - rightPadding) {
            return maxBucket;
        } else {
            return Math.round(((x - leftPadding) / effectiveWidth) * maxBucket);
        }
    }

    private void initializeDrawingTools() {
        primaryPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        primaryPaint.setColor(primaryColor);
        primaryPaint.setStyle(Paint.Style.FILL);

        secondaryPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        secondaryPaint.setColor(secondaryColor);
        secondaryPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        primaryLine.left = leftCircleX;
        primaryLine.right = rightCircleX;

        canvas.drawRect(secondaryLine, secondaryPaint);
        canvas.drawRect(primaryLine, primaryPaint);
        canvas.drawCircle(primaryLine.left, yCenter, leftCircleRadius, primaryPaint);
        canvas.drawCircle(primaryLine.right, yCenter, rightCircleRadius, primaryPaint);
    }

    @Override
    protected void onSizeChanged (int width, int height, int oldWidth, int oldHeight) {
        float lineRadius = LINE_RADIUS_DP * getResources().getDisplayMetrics().density;
        defaultCircleRadius = CIRCLE_RADIUS_DP * getResources().getDisplayMetrics().density;

        leftPadding = (float) getPaddingLeft() + defaultCircleRadius * 1.5f;
        rightPadding = (float) getPaddingRight() + defaultCircleRadius * 1.5f;

        float xPadding = leftPadding + rightPadding;
        float yPadding = (float)(getPaddingTop() + getPaddingBottom());

        effectiveWidth = (float)width - xPadding;
        float effectiveHeight = (float)height - yPadding;
        yCenter = getPaddingTop() + (effectiveHeight / 2f);

        leftCircleRadius = rightCircleRadius = defaultCircleRadius;

        leftCircleX = leftPadding;
        rightCircleX = leftPadding + effectiveWidth;

        secondaryLine = new RectF(leftPadding, yCenter - lineRadius, width - rightPadding, yCenter + lineRadius);
        primaryLine = new RectF(leftCircleX, yCenter - lineRadius, rightCircleX, yCenter + lineRadius);
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        invalidate();
    }

    public int getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(int primaryColor) {
        this.primaryColor = primaryColor;
        invalidate();
        requestLayout();
    }

    public int getSecondaryColor() {
        return secondaryColor;
    }

    public void setSecondaryColor(int secondaryColor) {
        this.secondaryColor = secondaryColor;
        invalidate();
        requestLayout();
    }

    public int getMaxBucket() {
        return maxBucket;
    }

    public void setMaxBucket(int maxBucket) {
        this.maxBucket = maxBucket;
    }

    public float getLeftCircleRadius() {
        return leftCircleRadius;
    }

    public void setLeftCircleRadius(float leftCircleRadius) {
        this.leftCircleRadius = leftCircleRadius;
    }

    public float getRightCircleRadius() {
        return rightCircleRadius;
    }

    public void setRightCircleRadius(float rightCircleRadius) {
        this.rightCircleRadius = rightCircleRadius;
    }

    private void notifyMinChange(boolean selected) {
        int bucket = getNearestBucket(leftCircleX);
        for(OnChangeListener changeListener : changeListeners) {
            changeListener.onMinChange(bucket, selected);
        }
    }

    private void notifyMaxChange(boolean selected) {
        int bucket = getNearestBucket(rightCircleX);
        for(OnChangeListener changeListener : changeListeners) {
            changeListener.onMaxChange(bucket, selected);
        }
    }

    public interface OnChangeListener {
        void onMinChange(int value, boolean selected);

        void onMaxChange(int value, boolean selected);
    }

    public void registerChangeListener(OnChangeListener changeListener) {
        changeListeners.add(changeListener);
    }
}
