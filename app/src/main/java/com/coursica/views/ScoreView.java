package com.coursica.views;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.PathInterpolator;

import com.coursica.helpers.Animatable;

public class ScoreView extends Animatable implements ValueAnimator.AnimatorUpdateListener {
    private static final String TAG = "ScoreView";
    float angle;
    Paint bigTextPaint;
    Paint smallTextPaint;
    Paint arcPaint;
    ObjectAnimator animator;
    RectF oval;
    float strokeWidth;
    Rect bigTextBounds;
    Rect smallTextBounds;

    String scoreText = "5.0";
    String descriptionText = "Overall";

    public ScoreView(Context context, AttributeSet attrs) {
        super(context, attrs);

        angle = 0F;
        strokeWidth = 50;

        bigTextBounds = new Rect();
        bigTextPaint = new TextPaint();
        bigTextPaint.setStyle(Paint.Style.FILL);
        bigTextPaint.setColor(0xff000000);
        bigTextPaint.setTextSize(200F);
        bigTextPaint.setAntiAlias(true);
        bigTextPaint.setTextAlign(Paint.Align.CENTER);
        bigTextPaint.getTextBounds(scoreText, 0, scoreText.length(), bigTextBounds);

        smallTextBounds = new Rect();
        smallTextPaint = new TextPaint();
        smallTextPaint.setStyle(Paint.Style.FILL);
        smallTextPaint.setColor(0xff000000);
        smallTextPaint.setTextSize(50);
        smallTextPaint.setAntiAlias(true);
        smallTextPaint.setTextAlign(Paint.Align.CENTER);
        smallTextPaint.getTextBounds(descriptionText, 0, descriptionText.length(), smallTextBounds);

        arcPaint = new Paint();
        arcPaint.setStyle(Paint.Style.STROKE);
        arcPaint.setColor(0xFF33B5E5);
        arcPaint.setStrokeWidth(strokeWidth);
        arcPaint.setAntiAlias(true);

        oval = new RectF(0f, 0f, 0f, 0f);

        animator = ObjectAnimator.ofFloat(this, "angle", 0F, 360F);
        animator.setDuration(2000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            animator.setInterpolator(new PathInterpolator(0.25F, 0.1F, 0.25F, 1F));
        } else {
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
        }
        animator.addUpdateListener(this);
    }

    public void runAnimation() {
        animator.start();
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public String getScoreText() {
        return scoreText;
    }

    public void set(float score, String descriptionText, int color) {
        this.scoreText = String.valueOf((Math.round(score * 10.0f))/10.0f);
        this.descriptionText = descriptionText;
        arcPaint.setColor(color);
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        int left = getPaddingLeft();
        int top = getPaddingTop();
        int right = getWidth() - getPaddingRight();
        int bottom = getHeight() - getPaddingBottom();

        float width = right - left;
        float height = bottom - top;
        float difference = height - width;
        if(difference < 0.001f) {
            left += Math.abs(difference) / 2f;
            right -=  Math.abs(difference) / 2f;
        } else if (difference > 0.01f) {
            top +=  Math.abs(difference) / 2f;
            bottom -=  Math.abs(difference) / 2f;
        }

        strokeWidth = (right - left) / 14F;

        oval = new RectF(left + strokeWidth/2F, top + strokeWidth/2F, right - strokeWidth/2F, bottom - strokeWidth/2F);
        arcPaint.setStrokeWidth(strokeWidth);

        bigTextPaint.setTextSize(200F);
        bigTextPaint.getTextBounds("5.0", 0, "5.0".length(), bigTextBounds);

        smallTextPaint.setTextSize(50);
        smallTextPaint.getTextBounds("Overall", 0, "Overall".length(), smallTextBounds);

        float bigTextSize = bigTextPaint.getTextSize() * (oval.width() / 2F) / bigTextBounds.width();
        float smallTextSize = smallTextPaint.getTextSize() * (oval.width() / 3F) / smallTextBounds.width();

        bigTextPaint.setTextSize(bigTextSize);
        bigTextPaint.getTextBounds(scoreText, 0, scoreText.length(), bigTextBounds);
        smallTextPaint.setTextSize(smallTextSize);
        smallTextPaint.getTextBounds(descriptionText, 0, descriptionText.length(), smallTextBounds);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float centerX = oval.centerX();
        float centerY = oval.centerY();

        canvas.drawText(scoreText, centerX, centerY + (bigTextBounds.height() / 2F) - smallTextBounds.height(), bigTextPaint);
        canvas.drawText(descriptionText, centerX, centerY + (bigTextBounds.height() / 2F) + smallTextBounds.height(), smallTextPaint);
        canvas.drawArc(oval, 270F, angle, false, arcPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int defaultSize = (int) getResources().getDisplayMetrics().density * 130;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY && heightMode == MeasureSpec.EXACTLY) {
            width = widthSize;
            height = heightSize;
        } else if (widthMode == MeasureSpec.EXACTLY && heightMode == MeasureSpec.AT_MOST) {
            width = widthSize;
            height = Math.min(defaultSize, Math.min(widthSize, heightSize));
        } else if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.EXACTLY) {
            width = Math.min(defaultSize, Math.min(widthSize, heightSize));
            height = heightSize;
        } else if (widthMode == MeasureSpec.EXACTLY && heightMode == MeasureSpec.UNSPECIFIED) {
            width = widthSize;
            height = Math.min(defaultSize, widthSize);
        } else if (widthMode == MeasureSpec.UNSPECIFIED && heightMode == MeasureSpec.EXACTLY) {
            width = Math.min(defaultSize, heightSize);
            height = heightSize;
        } else if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.AT_MOST) {
            int size = Math.min(defaultSize, Math.min(widthSize, heightSize));
            width = height = size;
        } else if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.UNSPECIFIED) {
            width = height = Math.min(defaultSize, widthSize);
        } else if (widthMode == MeasureSpec.UNSPECIFIED && heightMode == MeasureSpec.AT_MOST) {
            width = height = Math.min(defaultSize, heightSize);
        } else {
            width = height = defaultSize;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        invalidate();
    }
}
