package com.coursica.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import static com.coursica.views.BarView.Label.NONE;

public class VerticalBarView extends BarView {
    private static final String TAG = "VerticalBarView";

    private static final float TEXT_SCALE = 0.65F;
    private static final float TEXT_OFFSET = 0.5F;

    float left, bottom, right, circleRadius, totalHeight;

    public VerticalBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void recalculateDimensions() {
        left = getPaddingLeft();
        right = getWidth() - getPaddingRight();
        float width = right - left;
        circleRadius = width / 2;

        // determine text size
        if(labelType != NONE && textSize < 0) {
            textPaint.setTextSize(100F);
            String text = getFinalLabel();
            textPaint.getTextBounds(text, 0, text.length(), textBounds);

            float textSize = TEXT_SCALE * textPaint.getTextSize() * width / textBounds.width();

            textPaint.setTextSize(textSize);
            textPaint.getTextBounds(text, 0, text.length(), textBounds);
        }

        bottom = getHeight() - getPaddingBottom() - circleRadius;
        float endTop = getPaddingTop() + circleRadius;
        if(labelType != NONE) {
            endTop += textBounds.height() + TEXT_OFFSET * circleRadius;
        }
        totalHeight = bottom - endTop;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float centerX = left + circleRadius;
        float top = bottom - fractionFilled * totalHeight;

        canvas.drawCircle(centerX, bottom, circleRadius, paint);
        canvas.drawRect(left, top, right, bottom, paint);
        canvas.drawCircle(centerX, top, circleRadius, paint);
        if(labelType != NONE) {
            String text = getLabel();
            float textBaselineY = top - circleRadius - TEXT_OFFSET * circleRadius;
            canvas.drawText(text, centerX, textBaselineY, textPaint);
        }
    }
}
