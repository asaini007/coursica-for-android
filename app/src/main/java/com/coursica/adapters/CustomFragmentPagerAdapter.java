package com.coursica.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.coursica.fragments.CommentsFragment;
import com.coursica.fragments.ScoresFragment;
import com.coursica.fragments.SummaryFragment;

public class CustomFragmentPagerAdapter extends FragmentPagerAdapter {
    private final String tabTitles[] = {"Summary", "Q Scores", "Q Comments"};
    public static final int NUM_PAGES = 3;
    String number, shortField, title;
    FragmentManager fm;

    public CustomFragmentPagerAdapter(FragmentManager fm, String number, String shortField, String title) {
        super(fm);

        this.fm = fm;
        this.number = number;
        this.shortField = shortField;
        this.title = title;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return SummaryFragment.newInstance(number, shortField, title);
            case 1: return ScoresFragment.newInstance(number, shortField, title);
            case 2: return CommentsFragment.newInstance(number, shortField, title);
            default: return null;
        }
    }

    @Override
    public int getItemPosition(Object item) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}