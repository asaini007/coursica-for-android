package com.coursica.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.coursica.coursica.R;

import java.util.List;

public class CommentListAdapter extends BaseAdapter {
    Context context;
    List<String> data;

    public CommentListAdapter(Context context, List<String> comments) {
        this.context = context;
        this.data = comments;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public String getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_comment, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.comment_text_view)).setText(data.get(position));
        return convertView;
    }
}