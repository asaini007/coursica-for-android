package com.coursica.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.coursica.coursica.R;
import com.coursica.helpers.Color;
import com.coursica.model.Course;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CourseListAdapter extends BaseAdapter {
    public static final int[] SIZE_BUCKETS = {5, 10, 20, 40, 100, 200, 1000};

    Context context;
    Map<Integer, List<Double>> sizeBucketToScoreList;
    List<Course> data;

    public CourseListAdapter(Context context, List<Course> originalData, List<Course> data) {
        this.context = context;
        this.data = data;
        this.sizeBucketToScoreList = new HashMap<>();

        // initialize map
        for(Course course : originalData) {
            int sizeBucket = getSizeBucket(course.getEnrollment());
            if(sizeBucketToScoreList.containsKey(sizeBucket)) {
                sizeBucketToScoreList.get(sizeBucket).add(course.getOverall());
            } else {
                List<Double> overallScores = new ArrayList<>();
                overallScores.add(course.getOverall());
                sizeBucketToScoreList.put(sizeBucket, overallScores);
            }
        }

        // sort lists
        for(int key : sizeBucketToScoreList.keySet()) {
            Collections.sort(sizeBucketToScoreList.get(key));
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Course getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_course, parent, false);

            holder = new ViewHolder();
            holder.shortField = (TextView) convertView.findViewById(R.id.listItemShortField);
            holder.number = (TextView) convertView.findViewById(R.id.listItemNumber);
            holder.title = (TextView) convertView.findViewById(R.id.listItemTitle);
            holder.color = convertView.findViewById(R.id.overall_color);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Course course = data.get(position);
        holder.shortField.setText(course.getShortField());
        holder.number.setText(course.getNumber());
        holder.title.setText(course.getTitle());
        holder.color.setBackgroundColor(Color.getColorPercentile(context, getPercentile(course)));
        return convertView;
    }

    private int getPercentile(Course course) {
        double overallScore = course.getOverall();
        List<Double> overallScores = sizeBucketToScoreList.get(getSizeBucket(course.getEnrollment()));
        int numBelow = 0;
        while(overallScores.get(numBelow) < overallScore) {
            numBelow++;
        }
        return numBelow * 100 / overallScores.size();
    }

    private int getSizeBucket(int enrollment) {
        for(int sizeBucket : SIZE_BUCKETS) {
            if(enrollment <= sizeBucket) {
                return sizeBucket;
            }
        }
        return 0;
    }

    class ViewHolder {
        TextView shortField;
        TextView number;
        TextView title;
        View color;
    }
}
