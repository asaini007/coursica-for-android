package com.coursica.helpers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public abstract class Animatable extends View {

    public Animatable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public abstract void runAnimation();
}
