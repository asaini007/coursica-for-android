package com.coursica.helpers;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.coursica.coursica.R;

public final class Color {
    public static int getColorPercentile(Context context, int percentile) {
        if(percentile > 90)
            return ContextCompat.getColor(context, R.color.primary);
        else if(percentile > 80)
            return ContextCompat.getColor(context, R.color.second);
        else if(percentile > 70)
            return ContextCompat.getColor(context, R.color.third);
        else if(percentile > 60)
            return ContextCompat.getColor(context, R.color.fourth);
        else if(percentile > 50)
             return ContextCompat.getColor(context, R.color.fifth);
        else if(percentile > 40)
            return ContextCompat.getColor(context, R.color.sixth);
        else if(percentile > 30)
            return ContextCompat.getColor(context, R.color.seventh);
        else if(percentile > 20)
            return ContextCompat.getColor(context, R.color.eighth);
        else if(percentile > 10)
            return ContextCompat.getColor(context, R.color.ninth);
        else if(percentile > 0)
            return ContextCompat.getColor(context, R.color.tenth);
        else
            return ContextCompat.getColor(context, R.color.light_gray);
    }

    public static int getColorBaseline(Context context, float score, float baseline) {
        if(baseline < 0f)
            return getColor(context, score);
        float difference = score - baseline;
        if(difference > 0.3f) {
            return ContextCompat.getColor(context, R.color.best);
        } else if(difference > 0.15) {
            return ContextCompat.getColor(context, R.color.second_best);
        } else if(difference < -0.3f) {
            return ContextCompat.getColor(context, R.color.worst);
        } else if(difference < -0.15) {
            return ContextCompat.getColor(context, R.color.second_worst);
        } else {
            return ContextCompat.getColor(context, R.color.medium);
        }
    }

    public static int getInverseColorBaseline(Context context, float score, float baseline) {
        if(baseline < 0f)
            return getColorInverse(context, score);
        float difference = score - baseline;
        if(difference > 0.3f) {
            return ContextCompat.getColor(context, R.color.worst);
        } else if(difference > 0.15) {
            return ContextCompat.getColor(context, R.color.second_worst);
        } else if(difference < -0.3f) {
            return ContextCompat.getColor(context, R.color.best);
        } else if(difference < -0.15) {
            return ContextCompat.getColor(context, R.color.second_best);
        } else {
            return ContextCompat.getColor(context, R.color.medium);
        }
    }

    public static int getColor(Context context, float score) {
        if(score > 4.5) {
            return ContextCompat.getColor(context, R.color.best);
        } else if(score > 4.2) {
            return ContextCompat.getColor(context, R.color.second_best);
        } else if(score > 3.9) {
            return ContextCompat.getColor(context, R.color.medium);
        } else if(score > 3.6) {
            return ContextCompat.getColor(context, R.color.second_worst);
        } else {
            return ContextCompat.getColor(context, R.color.worst);
        }
    }

    public static int getColorInverse(Context context, float score) {
        if(score < 2) {
            return ContextCompat.getColor(context, R.color.best);
        } else if(score < 2.5) {
            return ContextCompat.getColor(context, R.color.second_best);
        } else if(score < 3) {
            return ContextCompat.getColor(context, R.color.medium);
        } else if(score < 3.5) {
            return ContextCompat.getColor(context, R.color.second_worst);
        } else {
            return ContextCompat.getColor(context, R.color.worst);
        }
    }
}
