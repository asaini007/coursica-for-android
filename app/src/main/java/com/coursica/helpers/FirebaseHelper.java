package com.coursica.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.coursica.coursica.R;
import com.coursica.model.Course;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;

public class FirebaseHelper {
    private static final String forbiddenCharacters[] = {".", "#", "$", "/", "[", "]"};
    private static final Map<String, String> shortFieldMap;
    private static final String TAG = "FireBaseHelper";

    static {
        shortFieldMap = new HashMap<>();
        shortFieldMap.put("AESTHINT", "AESTH&INTP");
        shortFieldMap.put("AMSTDIES", "AMSTUDIES");
        shortFieldMap.put("CULTBLF", "CULTR&BLF");
        shortFieldMap.put("EMREAS", "E&M-REASON");
        shortFieldMap.put("ETHRSON", "ETH-REASON");
        shortFieldMap.put("IMMUN", "IMUNOL");
        shortFieldMap.put("MODMDEST", "MODMIDEAST");
        shortFieldMap.put("SCILIVSY", "SCI-LIVSYS");
        shortFieldMap.put("SCIPHUNV", "SCI-PHYUNV");
        shortFieldMap.put("SOCWORLD", "SOC-WORLD");
        shortFieldMap.put("TDM", "DRAMA");
    }

    public static String replaceShortField(String shortField) {
        if(shortFieldMap.containsKey(shortField)) {
            return shortFieldMap.get(shortField);
        } else {
            return shortField;
        }
    }

    public static String replaceForbidden(String s) {
        String toReturn = s;
        for (int i = 0; i < forbiddenCharacters.length; i++) {
            toReturn = toReturn.replace(forbiddenCharacters[i], "&" + i + "&");
        }
        return toReturn;
    }

    public static String restoreForbidden(String s) {
        String toReturn = s;
        for (int i = 0; i < forbiddenCharacters.length; i++) {
            toReturn = toReturn.replace("&" + i + "&", forbiddenCharacters[i]);
        }
        return toReturn;
    }

    public static void saveSearchInFirebase(final Context context, final String searchTerm) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.prefs_file), Context.MODE_PRIVATE);
        String username = prefs.getString(context.getString(R.string.pref_username), null);
        Firebase ref = new Firebase("https://glaring-heat-9505.firebaseio.com/searches")
                .child(replaceForbidden(username));
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                addSearch(context, dataSnapshot, searchTerm);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private static void addSearch(Context context, DataSnapshot searchListSnapshot, String searchTerm) {
        String numChildren = String.valueOf(searchListSnapshot.getChildrenCount());
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.prefs_file), Context.MODE_PRIVATE);
        String username = prefs.getString(context.getString(R.string.pref_username), null);
        Firebase ref = new Firebase("https://glaring-heat-9505.firebaseio.com/searches")
                .child(replaceForbidden(username))
                .child(replaceForbidden(numChildren));
        ref.setValue(searchTerm);
    }

    public static String courseChildName(String shortField, String number) {
        return replaceForbidden(replaceShortField(shortField) + " " + number).toLowerCase();
    }

    public static String courseChildName(Course course) {
        return replaceForbidden(replaceShortField(course.getShortField()) + " " + course.getNumber()).toLowerCase();
    }

    public static List<TermData> getTermData(DataSnapshot courseSnapshot) {
        List<TermData> termData = new ArrayList<>();

        for (DataSnapshot termSnapshot : courseSnapshot.getChildren()) {
            if (termSnapshot.hasChild("year") && termSnapshot.hasChild("term")) {
                termData.add(new TermData(termSnapshot));
            }
        }

        return termData;
    }

    public static Firebase getCourseRef(String childName) {
        return new Firebase("https://glaring-heat-9505.firebaseio.com/QData/1_27_2016")
                .child(replaceForbidden(childName));
    }

    public static List<Course> getListCourses(DataSnapshot listSnapshot) {
        Realm realm = Realm.getDefaultInstance();
        List<Course> courses = new ArrayList<>();
        for(DataSnapshot courseSnapshot : listSnapshot.getChildren()) {
            if(courseSnapshot.exists() && courseSnapshot.hasChild("number")
                    && courseSnapshot.hasChild("shortField") && courseSnapshot.hasChild("title")) {
                String number = restoreForbidden(courseSnapshot.child("number").getValue(String.class));
                String shortField = restoreForbidden(courseSnapshot.child("shortField").getValue(String.class));
                String title = restoreForbidden(courseSnapshot.child("title").getValue(String.class));
                Course course = realm.where(Course.class)
                        .equalTo("number", number)
                        .equalTo("shortField", shortField)
                        .equalTo("title", title)
                        .findFirst();
                courses.add(course);
            }
        }
        return courses;
    }

    public static void addCourseToShopping(Context context, Course course) {
        Firebase shoppingCourseRef = getShoppingListRef(context).child(courseChildName(course));
        shoppingCourseRef.child("number").setValue(replaceForbidden(course.getNumber()));
        shoppingCourseRef.child("shortField").setValue(replaceForbidden(course.getShortField()));
        shoppingCourseRef.child("title").setValue(replaceForbidden(course.getTitle()));
    }

    public static void removeCourseFromShopping(Context context, Course course) {
        Firebase shoppingCourseRef = getShoppingListRef(context).child(courseChildName(course));
        shoppingCourseRef.setValue(null);
    }

    public static Firebase getShoppingListRef(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.prefs_file), Context.MODE_PRIVATE);
        String username = prefs.getString(context.getString(R.string.pref_username), null);
        return new Firebase("https://glaring-heat-9505.firebaseio.com/lists")
                .child(replaceForbidden(username))
                .child("Shopping");
    }

    private static float toFloat(Object obj) {
        Number n = (Number) obj;
        return n.floatValue();
    }

    private static int toInt(Object obj) {
        Number n = (Number) obj;
        return n.intValue();
    }

    // returns average if both values exist, the value if only one does, or -1f if neither do
    private static float average(DataSnapshot snapshot1, DataSnapshot snapshot2) {
        if(snapshot1.exists() && snapshot2.exists()) {
            return ((toFloat(snapshot1.getValue()) + toFloat(snapshot2.getValue())) / 2f);
        } else if(snapshot1.exists()) {
            return toFloat(snapshot1.getValue());
        } else if(snapshot2.exists()) {
            return toFloat(snapshot2.getValue());
        } else {
            return  -1f;
        }
    }

    public static class TermData {
        private DataSnapshot termSnapshot;
        private String year, term;
        private float overallScore, overallBaseline;
        private int[] overallBreakdown;
        private int[] workloadBreakdown;
        private float workloadScore, workloadBaseline;
        private List<InstructorData> instructors;

        public TermData(DataSnapshot termSnapshot) {
            this.termSnapshot = termSnapshot;

            setYear();
            setTerm();

            setOverallScore();
            setOverallBreakdown();
            setOverallBaseline();

            setWorkloadScore();
            setWorkloadBreakdown();
            setWorkloadBaseline();

            setFaculty();
        }

        private void setYear() {
            year = termSnapshot.hasChild("year") ? termSnapshot.child("year").getValue(String.class) : "";
        }

        private void setTerm() {
            term = termSnapshot.hasChild("term") ? termSnapshot.child("term").getValue(String.class) : "";
        }

        private void setOverallScore() {
            DataSnapshot overallSnapShot = termSnapshot.child("responses/Course Overall/mean");
            overallScore = overallSnapShot.exists() ? toFloat(overallSnapShot.getValue()) : -1.0f;
        }

        private void setOverallBreakdown() {
            DataSnapshot breakdownSnapshot = termSnapshot.child("responses/Course Overall/breakdown");
            if(breakdownSnapshot.exists()) {
                overallBreakdown = new int[5];
                for(int i = 0; i < 5; i++) {
                    DataSnapshot countSnapshot = breakdownSnapshot.child(String.valueOf(i));
                    overallBreakdown[i] = countSnapshot.exists() ? toInt(countSnapshot.getValue()) : 0;
                }
            } else {
                overallBreakdown = null;
            }
        }

        private void setOverallBaseline() {
            DataSnapshot groupSnapshot = termSnapshot.child("responses/Course Overall/baselines/single_term/group");
            DataSnapshot sizeSnapshot = termSnapshot.child("responses/Course Overall/baselines/single_term/size");
            overallBaseline = average(groupSnapshot, sizeSnapshot);
        }

        private void setWorkloadScore() {
            DataSnapshot workloadSnapshot = termSnapshot.child("responses/Workload (hours per week)/mean");
            workloadScore = workloadSnapshot.exists() ? toFloat(workloadSnapshot.getValue()) : -1.0f;
        }

        private void setWorkloadBreakdown() {
            DataSnapshot breakdownSnapshot = termSnapshot.child("responses/Workload (hours per week)/breakdown");
            if(breakdownSnapshot.exists()) {
                workloadBreakdown = new int[5];
                for(int i = 0; i < 5; i++) {
                    DataSnapshot countSnapshot = breakdownSnapshot.child(String.valueOf(i));
                    workloadBreakdown[i] = countSnapshot.exists() ? toInt(countSnapshot.getValue()) : 0;
                }
            } else {
                workloadBreakdown = null;
            }
        }

        private void setWorkloadBaseline() {
            DataSnapshot groupSnapshot = termSnapshot.child("responses/Workload (hours per week)/baselines/single_term/group");
            DataSnapshot sizeSnapshot = termSnapshot.child("responses/Workload (hours per week)/baselines/single_term/size");
            workloadBaseline = average(groupSnapshot, sizeSnapshot);
        }

        private void setFaculty() {
            instructors = new ArrayList<>();

            for(DataSnapshot instructorSnapshot : termSnapshot.child("faculty").getChildren()) {
                instructors.add(new InstructorData(instructorSnapshot));
            }
        }

        public String getYear() {
            return year;
        }

        public String getTerm() {
            return term;
        }

        public float getOverallScore() {
            return overallScore;
        }

        public int[] getOverallBreakdown() {
            return overallBreakdown;
        }

        public float getOverallBaseline() {
            return overallBaseline;
        }

        public float getWorkloadScore() {
            return workloadScore;
        }

        public float getWorkloadBaseline() {
            return workloadBaseline;
        }

        public int[] getWorkloadBreakdown() {
            return workloadBreakdown;
        }

        public List<InstructorData> getInstructors() {
            return instructors;
        }
    }

    public static class InstructorData {
        private String name;
        private float mean = -1F;
        private float baseline = -1F;

        private List<CategoryData> categories;

        public InstructorData(DataSnapshot instructorSnapshot) {
            name = instructorSnapshot.getKey();

            categories = new ArrayList<>();
            for(DataSnapshot categorySnapshot : instructorSnapshot.getChildren()) {
                categories.add(new CategoryData(categorySnapshot));
            }

            for(CategoryData categoryData : categories) {
                if(categoryData.categoryName.equals("Instructor Overall")) {
                    mean = categoryData.mean;
                    baseline = categoryData.baseline;
                    break;
                }
            }
        }

        public String getName() {
            return name;
        }

        public float getMean() {
            return mean;
        }

        public float getBaseline() {
            return baseline;
        }

        public List<CategoryData> getCategories() {
            return categories;
        }
    }

    public static class CategoryData {
        public final String categoryName;
        public final float mean;
        public final float baseline;

        public CategoryData(DataSnapshot categorySnapshot) {
            categoryName = categorySnapshot.getKey();
            DataSnapshot meanSnapshot = categorySnapshot.child("mean");
            mean = meanSnapshot.exists() ? toFloat(meanSnapshot.getValue()) : -1f;

            DataSnapshot groupSnapshot = categorySnapshot.child("baselines/single_term/group");
            DataSnapshot sizeSnapshot = categorySnapshot.child("baselines/single_term/size");
            baseline = average(groupSnapshot, sizeSnapshot);
        }
    }
}
