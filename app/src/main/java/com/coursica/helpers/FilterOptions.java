package com.coursica.helpers;

import java.util.HashSet;
import java.util.Set;

public class FilterOptions {
    public enum Term {FALL, SPRING, BOTH}
    public enum Level {UNDERGRAD, GRAD, BOTH}

    Term term;
    Level level;
    double minOverall;
    double maxOverall;
    double minWorkload;
    double maxWorkload;
    int minEnrollment;
    int maxEnrollment;
    Set<String> genEds;

    public FilterOptions() {
        term = Term.BOTH;
        level = Level.BOTH;
        minOverall = -1.0;
        maxOverall = 5.0;
        minWorkload = -1.0;
        maxWorkload = 5.0;
        minEnrollment = 0;
        maxEnrollment = 1000;
        genEds = new HashSet<>();
    }

    public boolean equals(FilterOptions that) {
        return (this.term == that.term
                && this.level == that.level
                && this.minOverall == that.minOverall
                && this.maxOverall == that.maxOverall
                && this.minWorkload == that.minWorkload
                && this.maxWorkload == that.maxWorkload
                && this.minEnrollment == that.minEnrollment
                && this.maxEnrollment == that.maxEnrollment
                && this.genEds.equals(that.genEds));
    }

    public void copy(FilterOptions that) {
        this.term = that.term;
        this.level = that.level;
        this.minOverall = that.minOverall;
        this.maxOverall = that.maxOverall;
        this.minWorkload = that.minWorkload;
        this.maxWorkload = that.maxWorkload;
        this.minEnrollment = that.minEnrollment;
        this.maxEnrollment = that.maxEnrollment;
        this.genEds.clear();
        this.genEds.addAll(that.genEds);
    }

//    private boolean compareAndAssign(String fieldName, Object newValue) throws NoSuchFieldException, IllegalAccessException {
//        Field field = this.getClass().getDeclaredField(fieldName);
//        Object value = field.get(this);
//        if(value == newValue)
//    }

    public boolean setTerm(Term term) {
        boolean changed = this.term != term;
        this.term = term;
        return changed;
    }

    public Term getTerm() {
        return term;
    }

    public Level getLevel() {
        return level;
    }

    public boolean setLevel(Level level) {
        boolean changed = this.level != level;
        this.level = level;
        return changed;
    }

    public double getMinOverall() {
        return minOverall;
    }

    public boolean setMinOverall(double minOverall) {
        boolean changed = this.minOverall != minOverall;
        this.minOverall = minOverall;
        return changed;
    }

    public double getMaxOverall() {
        return maxOverall;
    }

    public boolean setMaxOverall(double maxOverall) {
        boolean changed = this.maxOverall != maxOverall;
        this.maxOverall = maxOverall;
        return changed;
    }

    public double getMinWorkload() {
        return minWorkload;
    }

    public boolean setMinWorkload(double minWorkload) {
        boolean changed = this.minWorkload != minWorkload;
        this.minWorkload = minWorkload;
        return changed;
    }

    public double getMaxWorkload() {
        return maxWorkload;
    }

    public boolean setMaxWorkload(double maxWorkload) {
        boolean changed = this.maxWorkload != maxWorkload;
        this.maxWorkload = maxWorkload;
        return changed;
    }

    public int getMinEnrollment() {
        return minEnrollment;
    }

    public boolean setMinEnrollment(int minEnrollment) {
        boolean changed = this.minEnrollment != minEnrollment;
        this.minEnrollment = minEnrollment;
        return changed;
    }

    public int getMaxEnrollment() {
        return maxEnrollment;
    }

    public boolean setMaxEnrollment(int maxEnrollment) {
        boolean changed = this.maxEnrollment != maxEnrollment;
        this.maxEnrollment = maxEnrollment;
        return changed;
    }

    // if genEds contains genEd, remove it and return false; else, add it and return true
    public boolean toggleGenEd(String genEd) {
        if (genEds.contains(genEd)) {
            genEds.remove(genEd);
            return false;
        } else {
            genEds.add(genEd);
            return true;
        }
    }

    public Set<String> getGenEds() {
        return genEds;
    }
}
