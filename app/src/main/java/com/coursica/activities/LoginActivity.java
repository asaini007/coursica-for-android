package com.coursica.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.coursica.coursica.R;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    class MyJavaScriptInterface {
        @JavascriptInterface
        public void getValue(String username) {
            Log.d(TAG, "Username: " + username);
            logIn(username);
        }
    }

    private void logIn(String username) {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs_file), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(getString(R.string.pref_signed_in), true);
        editor.putString(getString(R.string.pref_username), username);
        editor.commit();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.login_toolbar);
        toolbar.setTitle("Login");
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }

        final WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.addJavascriptInterface(new MyJavaScriptInterface(), "interface");
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("https://portal.my.harvard.edu/")) {
                    myWebView.loadUrl("javascript:interface.getValue(document.getElementById('username').value);");
                    return true;
                } else {
                    return false;
                }
            }
        });
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        myWebView.loadUrl("https://portal.my.harvard.edu/psp/hrvihprd/?cmd=start&quot;");
    }
}
