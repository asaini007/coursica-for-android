package com.coursica.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.coursica.adapters.CourseListAdapter;
import com.coursica.coursica.R;
import com.coursica.model.Course;
import com.coursica.model.Faculty;
import com.firebase.client.Firebase;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

public class FacultyActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "FacultyActivity";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty);

        Uri uri =  getIntent().getData();
        List<String> facultyName = uri.getPathSegments();
        String first = facultyName.get(0);
        String last = facultyName.get(1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.faculty_toolbar);
        toolbar.setTitle(first + " " + last);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }

        Realm realm;
        try {
            realm = Realm.getDefaultInstance();
        } catch(NullPointerException e) {
            Firebase.setAndroidContext(this);
            RealmConfiguration realmConfig = new RealmConfiguration.Builder(this)
                    .name("courses.realm")
                    .build();
            Realm.setDefaultConfiguration(realmConfig);
            realm = Realm.getDefaultInstance();
        }
        RealmResults<Course> results = realm.allObjects(Course.class);
        String[] sortFieldNames = {"shortField", "integerNumber", "number"};
        Sort[] sortOrders = {Sort.ASCENDING, Sort.ASCENDING, Sort.ASCENDING};
        results.sort(sortFieldNames, sortOrders);

        List<Course> data = new ArrayList<>();
        for(Course course : results) {
            for(Faculty courseFaculty : course.getFaculty()) {
                if(courseFaculty.getFirst().equals(first) && courseFaculty.getLast().equals(last)) {
                    data.add(course);
                }
            }
        }

        List<Course> originalData = new ArrayList<>(results);
        BaseAdapter adapter = new CourseListAdapter(this, originalData, data);
        ListView listView = (ListView) findViewById(R.id.faculty_list_view);

        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String number = (String) ((TextView) view.findViewById(R.id.listItemNumber)).getText();
        String field = (String) ((TextView) view.findViewById(R.id.listItemShortField)).getText();
        String title = (String) ((TextView) view.findViewById(R.id.listItemTitle)).getText();

        Intent intent = new Intent(this, CourseActivity.class);
        intent.putExtra(MainActivity.COURSE_NUMBER, number);
        intent.putExtra(MainActivity.COURSE_SHORT_FIELD, field);
        intent.putExtra(MainActivity.COURSE_TITLE, title);

        startActivity(intent);
    }
}
