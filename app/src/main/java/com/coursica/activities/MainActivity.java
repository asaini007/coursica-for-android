package com.coursica.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.coursica.adapters.CourseListAdapter;
import com.coursica.coursica.R;
import com.coursica.helpers.FilterOptions;
import com.coursica.helpers.FilterOptions.Level;
import com.coursica.helpers.FilterOptions.Term;
import com.coursica.helpers.FirebaseHelper;
import com.coursica.model.Course;
import com.coursica.model.Faculty;
import com.coursica.model.GenEd;
import com.coursica.views.SliderView;
import com.firebase.client.Firebase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    public final static String COURSE_NUMBER = "com.coursica.coursica.NUMBER";
    public final static String COURSE_SHORT_FIELD = "com.coursica.coursica.FIELD";
    public final static String COURSE_TITLE = "com.coursica.coursica.TITLE";

    private static final String TAG = "MainActivity";

    private static final Map<String, String> abbreviationsMap;
    static {
        abbreviationsMap = new HashMap<>();
        abbreviationsMap.put("cs", "computer science");
        abbreviationsMap.put("ec", "economics");
        abbreviationsMap.put("er", "ethrson");
        abbreviationsMap.put("cb", "culture and belief");
        abbreviationsMap.put("ai", "aesthetic and interpretive understanding");
        abbreviationsMap.put("aiu", "aesthetic and interpretive understanding");
        abbreviationsMap.put("am", "applied math");
        abbreviationsMap.put("astro", "astronomy");
        abbreviationsMap.put("bio", "biology");
        abbreviationsMap.put("lit", "literature");
        abbreviationsMap.put("comp", "computer comparative");
        abbreviationsMap.put("sci", "science");
        abbreviationsMap.put("em", "empirical and mathematical reasoning");
        abbreviationsMap.put("eps", "earth and planetary sciences");
        abbreviationsMap.put("es", "engineering sciences");
        abbreviationsMap.put("pol", "policy politics");
        abbreviationsMap.put("psych", "psychology");
        abbreviationsMap.put("hum", "humanities");
        abbreviationsMap.put("hist", "history");
        abbreviationsMap.put("kor", "korean");
        abbreviationsMap.put("lat", "latin");
        abbreviationsMap.put("med", "medical");
        abbreviationsMap.put("stats", "statistics");
        abbreviationsMap.put("sls", "science of living systems");
        abbreviationsMap.put("spu", "science of the physical universe");
        abbreviationsMap.put("syst", "systems");
        abbreviationsMap.put("usw", "united states in the world");
        abbreviationsMap.put("ls", "life sciences");
    }

    private List<Course> originalData, prefilteredData, filteredData;
    private BaseAdapter adapter;
    private boolean showingFilter;
    private FilterOptions filterOptions;

    Spinner spinner;
    boolean isShopping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.login_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        spinner = (Spinner) findViewById(R.id.toolbar_spinner);
        final String[] data = getResources().getStringArray(R.array.toolbar_items);
        spinner.setAdapter(new CustomizedSpinnerAdapter(this, R.layout.spinner_item_toolbar_dropdown, data));
        spinner.setOnItemSelectedListener(this);

        setUpFilterPane();
        setUpOverallSlider();
        setUpWorkloadSlider();
        setUpEnrollmentSlider();
        initializeData();

        handleIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();

        Spinner spinner = (Spinner) findViewById(R.id.toolbar_spinner);
        if(spinner != null && spinner.getSelectedItemPosition() == 1) {
            setShoppingData();
        }
    }

    private void setUpFilterPane() {
        showingFilter = false;
        final ViewGroup rootLayout = (ViewGroup)findViewById(R.id.root_layout);
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                showingFilter = false;
                View filterPane = findViewById(R.id.filter_pane);
                filterPane.setTranslationY(-filterPane.getHeight());
                filterPane.setVisibility(View.VISIBLE);
                rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void setUpOverallSlider() {
        final TextView leftOverallScore = (TextView) findViewById(R.id.left_overall_score);
        final TextView rightOverallScore = (TextView) findViewById(R.id.right_overall_score);
        SliderView overallSlider = (SliderView) findViewById(R.id.overall_slider);
        overallSlider.registerChangeListener(new SliderView.OnChangeListener() {
            @Override
            public void onMinChange(int value, boolean selected) {
                double score = (value / 10.0) + 1.0;
                leftOverallScore.setText(String.valueOf(score));
                if (selected) {
                    if (score == 1.0) {
                        score = -1.0;
                    }
                    applyFilters(filterOptions.setMinOverall(score));
                }
            }

            @Override
            public void onMaxChange(int value, boolean selected) {
                double score = (value / 10.0) + 1.0;
                rightOverallScore.setText(String.valueOf(score));
                if (selected) {
                    applyFilters(filterOptions.setMaxOverall(score));
                }
            }
        });
    }

    private void setUpWorkloadSlider() {
        final TextView leftWorkloadScore = (TextView) findViewById(R.id.left_workload_score);
        final TextView rightWorkloadScore = (TextView) findViewById(R.id.right_workload_score);
        SliderView workloadSlider = (SliderView) findViewById(R.id.workload_slider);
        workloadSlider.registerChangeListener(new SliderView.OnChangeListener() {
            @Override
            public void onMinChange(int value, boolean selected) {
                double workload = (value / 10.0) + 1.0;
                leftWorkloadScore.setText(String.valueOf(workload));
                if (selected) {
                    if(workload == 1.0) {
                        workload = -1.0;
                    }
                    applyFilters(filterOptions.setMinWorkload(workload));
                }

            }

            @Override
            public void onMaxChange(int value, boolean selected) {
                double workload = (value / 10.0) + 1.0;
                rightWorkloadScore.setText(String.valueOf(workload));
                if (selected) {
                    applyFilters(filterOptions.setMaxWorkload(workload));
                }
            }
        });
    }

    private void setUpEnrollmentSlider() {
        final TextView leftEnrollment = (TextView) findViewById(R.id.left_enrollment);
        final TextView rightEnrollment = (TextView) findViewById(R.id.right_enrollment);
        SliderView enrollmentSlider = (SliderView) findViewById(R.id.enrollment_slider);
        enrollmentSlider.registerChangeListener(new SliderView.OnChangeListener() {
            @Override
            public void onMinChange(int value, boolean selected) {
                int enrollment = value + 1;
                leftEnrollment.setText(String.valueOf(enrollment));
                if (selected) {
                    if (enrollment == 1) {
                        enrollment = 0;
                    }
                    applyFilters(filterOptions.setMinEnrollment(enrollment));
                }
            }

            @Override
            public void onMaxChange(int value, boolean selected) {
                int enrollment;
                if (value == 249) {
                    enrollment = 1000;
                    rightEnrollment.setText("250+");
                } else {
                    enrollment = value + 1;
                    rightEnrollment.setText(String.valueOf(enrollment));
                }
                if (selected) {
                    applyFilters(filterOptions.setMaxEnrollment(enrollment));
                }
            }
        });
    }

    private void initializeData() {
        Firebase.setAndroidContext(this);

        SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs_file), MODE_PRIVATE);
        boolean signedIn = prefs.getBoolean(getString(R.string.pref_signed_in), false);
        if(!signedIn) {
            Intent intent = new Intent(this, SplashScreenActivity.class);
            finish();
            startActivity(intent);
        } else {
            copyBundledRealmFile();

            RealmConfiguration realmConfig = new RealmConfiguration.Builder(this)
                    .name("courses.realm")
                    .build();
            Realm.setDefaultConfiguration(realmConfig);

            Realm realm = Realm.getDefaultInstance();
            RealmResults<Course> results = realm.allObjects(Course.class);
            String[] sortFieldNames = {"shortField", "integerNumber", "number"};
            Sort[] sortOrders = {Sort.ASCENDING, Sort.ASCENDING, Sort.ASCENDING};
            results.sort(sortFieldNames, sortOrders);

            filterOptions = new FilterOptions();
            originalData = new ArrayList<>(results);
            prefilteredData = new ArrayList<>(results);
            filteredData = new ArrayList<>();
            isShopping = false;

            ListView listView = (ListView) findViewById(R.id.list_view);
            listView.setOnItemClickListener(this);
            listView.setAdapter(adapter = new CourseListAdapter(this, originalData, filteredData));

            applyFilters(true);
        }
    }

    private void setShoppingData() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE);
        Set<String> shoppingNumbers = prefs.getStringSet(getString(R.string.pref_shopping), new HashSet<String>());
        Realm realm = Realm.getDefaultInstance();
        prefilteredData.clear();
        for(String number : shoppingNumbers) {
            prefilteredData.add(realm.where(Course.class).equalTo("catalogNumber", number).findFirst());
        }
        applyFilters(true);
    }

    private void copyBundledRealmFile() {
        try {
            String outFileName = "courses.realm";
            InputStream inputStream = getAssets().open("courses.realm");
            File file = new File(getFilesDir(), outFileName);
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search_menu_item);

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (isShopping) {
                    setShoppingData();
                } else {
                    setOriginalData();
                }
                return true;
            }
        });

        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void setOriginalData() {
        prefilteredData.clear();
        prefilteredData.addAll(originalData);
        applyFilters(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_menu_item:
                return true;
            case R.id.filter_menu_item:
                toggleFilterPane();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0: isShopping = false;
                setOriginalData();
                break;
            case 1:
                isShopping = true;
                setShoppingData();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void toggleFilterPane() {
        View filterPane = findViewById(R.id.filter_pane);
        View listView = findViewById(R.id.list_view);
        if (showingFilter) {
            filterPane.animate().translationY(-filterPane.getHeight());
            listView.animate().translationY(0);
        } else {
            filterPane.animate().translationY(0);
            listView.animate().translationY(filterPane.getHeight());
        }
        showingFilter = !showingFilter;
    }

    private void applyFilters(boolean changed) {
        if(changed) {
            filteredData.clear();
            for (Course course : prefilteredData) {
                if ((filterOptions.getTerm() == Term.BOTH
                        || (filterOptions.getTerm() == Term.FALL && course.isFall())
                        || (filterOptions.getTerm() == Term.SPRING && course.isSpring()))
                        && (filterOptions.getLevel() == Level.BOTH
                        || toLevel(course.getLevel()) == Level.BOTH
                        || toLevel(course.getLevel()) == filterOptions.getLevel())
                        && course.getOverall() >= filterOptions.getMinOverall()
                        && course.getOverall() <= filterOptions.getMaxOverall()
                        && course.getWorkload() >= filterOptions.getMinWorkload()
                        && course.getWorkload() <= filterOptions.getMaxWorkload()
                        && course.getEnrollment() >= filterOptions.getMinEnrollment()
                        && course.getEnrollment() <= filterOptions.getMaxEnrollment()
                        && contains(course.getGenEds(), filterOptions.getGenEds())) {
                    filteredData.add(course);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    private boolean contains(RealmList<GenEd> courseGenEds, Set<String> filterGenEds) {
        if(filterGenEds.size() == 0) {
            return true;
        }
        for(GenEd genEd : courseGenEds) {
            if(filterGenEds.contains(genEd.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            FirebaseHelper.saveSearchInFirebase(this, query);
            executeSearch(query);
        }
    }

    private void executeSearch(String query) {
        String[] explodedQuery = query.toLowerCase().split(" ");

        List<String> replacedQuery = new ArrayList<>();
        for(String token : explodedQuery) {
            String[] tokenArray = token.split("[^a-zA-Z]");
            if(tokenArray.length > 0) {
                if (abbreviationsMap.get(tokenArray[0]) == null) {
                    replacedQuery.add(token);
                } else {
                    replacedQuery.add(abbreviationsMap.get(tokenArray[0]));
                    if(token.length() > tokenArray[0].length()) {
                        replacedQuery.add(token.substring(tokenArray[0].length()));
                    }
                }
            } else {
                replacedQuery.add(token);
            }
        }

        prefilteredData.clear();
        for(Course course : originalData) {
            boolean match = true;
            String title = course.getTitle().toLowerCase();
            String longField = course.getLongField().toLowerCase();
            String shortField = course.getShortField().toLowerCase();
            String number = course.getNumber().toLowerCase();
            List<Faculty> faculty = course.getFaculty();
            List<String> instructors = new ArrayList<>();
            for(Faculty f : faculty) {
                instructors.add(f.getFirst().toLowerCase());
                instructors.add(f.getLast().toLowerCase());
            }

            for(String token : replacedQuery) {
                if(!title.contains(token) && !longField.contains(token) && !shortField.equals(token)
                        && !number.contains(token) && !instructors.contains(token)) {
                    match = false;
                    break;
                }
            }
            if(match) {
                prefilteredData.add(course);
            }
        }
        applyFilters(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String number = (String) ((TextView) view.findViewById(R.id.listItemNumber)).getText();
        String field = (String) ((TextView) view.findViewById(R.id.listItemShortField)).getText();
        String title = (String) ((TextView) view.findViewById(R.id.listItemTitle)).getText();

        Intent intent = new Intent(this, CourseActivity.class);
        intent.putExtra(COURSE_NUMBER, number);
        intent.putExtra(COURSE_SHORT_FIELD, field);
        intent.putExtra(COURSE_TITLE, title);

        startActivity(intent);
    }

    public void onTermSelect(View view) {
        Term prevSelectedTerm = filterOptions.getTerm();
        boolean changed = false;

        ((Button) view).setTextColor(getResources().getColor(R.color.primary));
        switch(view.getId()) {
            case R.id.fall_button: changed = filterOptions.setTerm(Term.FALL);
                break;
            case R.id.spring_button: changed = filterOptions.setTerm(Term.SPRING);
                break;
            case R.id.both_terms_button: changed = filterOptions.setTerm(Term.BOTH);
        }

        if(changed) {
            switch (prevSelectedTerm) {
                case FALL:
                    ((Button) findViewById(R.id.fall_button)).setTextColor(getResources().getColor(R.color.unselected_gray_text));
                    break;
                case SPRING:
                    ((Button) findViewById(R.id.spring_button)).setTextColor(getResources().getColor(R.color.unselected_gray_text));
                    break;
                case BOTH:
                    ((Button) findViewById(R.id.both_terms_button)).setTextColor(getResources().getColor(R.color.unselected_gray_text));
            }
        }

        applyFilters(changed);
    }

    public void onLevelSelect(View view) {
        ((Button) view).setTextColor(getResources().getColor(R.color.primary));

        Level prevSelectedLevel = filterOptions.getLevel();
        boolean changed = false;

        switch(view.getId()) {
            case R.id.undergrad_button: changed = filterOptions.setLevel(Level.UNDERGRAD);
                break;
            case R.id.grad_button: changed = filterOptions.setLevel(Level.GRAD);
                break;
            case R.id.both_levels_button: changed = filterOptions.setLevel(Level.BOTH);
        }

        if(changed) {
            switch (prevSelectedLevel) {
                case UNDERGRAD:
                    ((Button) findViewById(R.id.undergrad_button)).setTextColor(getResources().getColor(R.color.unselected_gray_text));
                    break;
                case GRAD:
                    ((Button) findViewById(R.id.grad_button)).setTextColor(getResources().getColor(R.color.unselected_gray_text));
                    break;
                case BOTH:
                    ((Button) findViewById(R.id.both_levels_button)).setTextColor(getResources().getColor(R.color.unselected_gray_text));
            }
        }

        applyFilters(changed);
    }

    public void onGenEdSelect(View view) {
        String genEd = toGenEd(view.getId());
        boolean added = filterOptions.toggleGenEd(genEd);
        ViewGroup viewGroup = (ViewGroup) view;
        ImageView image = (ImageView) viewGroup.getChildAt(0);
        TextView text = (TextView) viewGroup.getChildAt(1);
        int color = added ? getResources().getColor(R.color.primary) : getResources().getColor(R.color.light_gray);
        image.setColorFilter(color);
        text.setTextColor(color);
        applyFilters(true);
    }

    private String toGenEd(int id) {
        switch (id) {
            case R.id.aiu_button: return "Aesthetic and Interpretive Understanding";
            case R.id.cb_button: return "Culture and Belief";
            case R.id.emr_button: return "Empirical and Mathematical Reasoning";
            case R.id.er_button: return "Ethical Reasoning";
            case R.id.sls_button: return "Science of Living Systems";
            case R.id.spu_button: return "Science of the Physical Universe";
            case R.id.socw_button: return "Societies of the World";
            case R.id.usw_button: return "United States in the World";
            default:
                return "";
        }
    }

    private static Term toTerm(String termString) {
        if (termString.toLowerCase().equals("fall"))
            return Term.FALL;
        if (termString.toLowerCase().equals("spring"))
            return Term.SPRING;
        return Term.BOTH;
    }

    private static Level toLevel(String level) {
        if(level.equals("Graduate")) {
            return Level.GRAD;
        } else if(level.equals("Undergraduate")) {
            return Level.UNDERGRAD;
        }
        return Level.BOTH;
    }

    public class CustomizedSpinnerAdapter extends ArrayAdapter<String> {
        private Context context;
        String[] data = null;

        public CustomizedSpinnerAdapter(Context context, int resource, String[] data) {
            super(context, resource, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.spinner_item_toolbar, parent, false);
            }
            TextView text1 = (TextView) convertView.findViewById(R.id.toolbar_spinner_dropdown_item_textview);
            text1.setText(data[position]);
            return convertView;
        }
    }
}
