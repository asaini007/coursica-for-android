package com.coursica.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;

import com.coursica.coursica.R;
import com.coursica.fragments.ScoresFragment;
import com.coursica.helpers.Animatable;
import com.coursica.views.BarView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DetailedBreakdownActivity extends AppCompatActivity {
    private static final String TAG = "DetailedBreakdownActvty";

    List<Animatable> animatables;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defailed_breakdown);

        // get course
        Intent intent = getIntent();
        String title = intent.getStringExtra(ScoresFragment.BREAKDOWN_TYPE);

        // set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.detailed_breakdown_toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }

        animatables = new ArrayList<>();
        final ScrollView layout = (ScrollView) findViewById(R.id.detailed_breakdown_scrollview);
        layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
//                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Log.d(TAG, "onGlobalLayout() called");
                Iterator<Animatable> iterator = animatables.iterator();
                while(iterator.hasNext()) {
                    Animatable animatable = iterator.next();
                    animatable.runAnimation();
                    iterator.remove();
                }
            }
        });

        int[] countData = intent.getIntArrayExtra(ScoresFragment.BREAKDOWN_DATA);
        String[] labelData = intent.getStringArrayExtra(ScoresFragment.BREAKDOWN_LABELS);
        String question = intent.getStringExtra(ScoresFragment.BREAKDOWN_QUESTION);

        int highestCount = 0;
        for(int i = 0; i < countData.length; i++) {
            if(countData[i] > highestCount) {
                highestCount = countData[i];
            }
        }

        float[] fractionData = new float[5];
        for(int i = 0; i < fractionData.length; i++) {
            fractionData[i] = countData[i] / (float) highestCount;
        }

        BarView barView1 = (BarView) findViewById(R.id.detailed_barview_1);
        barView1.setFractionToFill(fractionData[0]);
        barView1.setLabel(countData[0]);
        animatables.add(barView1);

        BarView barView2 = (BarView) findViewById(R.id.detailed_barview_2);
        barView2.setFractionToFill(fractionData[1]);
        barView2.setLabel(countData[1]);
        animatables.add(barView2);

        BarView barView3 = (BarView) findViewById(R.id.detailed_barview_3);
        barView3.setFractionToFill(fractionData[2]);
        barView3.setLabel(countData[2]);
        animatables.add(barView3);

        BarView barView4 = (BarView) findViewById(R.id.detailed_barview_4);
        barView4.setFractionToFill(fractionData[3]);
        barView4.setLabel(countData[3]);
        animatables.add(barView4);

        BarView barView5 = (BarView) findViewById(R.id.detailed_barview_5);
        barView5.setFractionToFill(fractionData[4]);
        barView5.setLabel(countData[4]);
        animatables.add(barView5);

        ((TextView) findViewById(R.id.breakdown_label_1)).setText(labelData[0]);
        ((TextView) findViewById(R.id.breakdown_label_2)).setText(labelData[1]);
        ((TextView) findViewById(R.id.breakdown_label_3)).setText(labelData[2]);
        ((TextView) findViewById(R.id.breakdown_label_4)).setText(labelData[3]);
        ((TextView) findViewById(R.id.breakdown_label_5)).setText(labelData[4]);

        ((TextView) findViewById(R.id.breakdown_question)).setText(question);
    }
}
