package com.coursica.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.coursica.adapters.CustomFragmentPagerAdapter;
import com.coursica.coursica.R;

public class CourseActivity extends AppCompatActivity {
    private static final String TAG = "CourseActivity";
    String number, shortField, title;
    int viewPagerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);

        // get course
        Intent intent = getIntent();
        number = intent.getStringExtra(MainActivity.COURSE_NUMBER);
        shortField = intent.getStringExtra(MainActivity.COURSE_SHORT_FIELD);
        title = intent.getStringExtra(MainActivity.COURSE_TITLE);

        // set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.course_toolbar);
        toolbar.setTitle(shortField + " " + number);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }

        // get the ViewPager and set its PagerAdapter so that it can display items
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPagerId = viewPager.getId();
        PagerAdapter adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager(), number, shortField, title);
        viewPager.setAdapter(adapter);

        // give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }
}
